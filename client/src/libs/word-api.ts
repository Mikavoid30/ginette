import axios from 'axios'

export const postSearchForm = async (
  data: any,
  filters: any,
  doNotSaveInHistory = false,
  endpoint = '/api/v1/words/search'
) => {
  const x = await axios.post(`${endpoint}`, {
    params: data,
    filters,
    options: {
      saveInHistory: !doNotSaveInHistory
    }
  })
  return x
}

export const getWordDetails = async (
  id: string,
  endpoint = '/api/v1/words/find/'
) => {
  try {
    const x = await axios.get(`${endpoint}`, { params: { id: id } })
    return x.data
  } catch (e) {
    return Promise.reject(e)
  }
}
