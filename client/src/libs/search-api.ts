import axios from 'axios'

export const fetchLatestSearches = async (
  nb = 5,
  endpoint = '/api/v1/search/latest'
) => {
  const x = await axios.get(`${endpoint}/${nb}`)
  return x
}

export const deleteSearchesHistory = async (endpoint = '/api/v1/search') => {
  await axios.delete(`${endpoint}`)
}
