import React from 'react'
import PresentationScreen from '../PresentationScreen/PresentationScreen'

import './Hero.scss'

type HeroProps = {}

function Hero (props: HeroProps): JSX.Element {
  return (
    <div className='Hero'>
      <div className='Hero_container'>
        <div className='Hero_title'>
          <div className='circles'>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
          <div className='circles circles--bottom'>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
          <h1>Recherche de mots par structures syntaxiques et phonèmes</h1>
        </div>
      </div>
    </div>
  )
}

export default Hero
