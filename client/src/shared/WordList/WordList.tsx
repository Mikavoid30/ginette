import React, { memo } from 'react'
import { wordSelectionData } from '../../redux/actions/words/word-selection'
import { Word } from '../../types/words'
import WordCard from '../WordCard/WordCard'
import './WordList.scss'

type WordListProps = {
  list: Word[]
  wordSelection: wordSelectionData[]
  onWordSelected?: (data: wordSelectionData) => any
  onWordUnSelected?: (data: wordSelectionData) => any
}

function WordList ({
  list,
  wordSelection = [],
  onWordSelected = x => x,
  onWordUnSelected = x => x
}: WordListProps) {
  // const devList = [
  //   {
  //     _id: '2',
  //     text: 'banane',
  //     pron: 'banan',
  //     freq: 2,
  //     aoa: 4,
  //     type: 'noun',
  //     structure: 'CV-CV-CV'
  //   },
  //   {
  //     _id: '3',
  //     text: 'banane',
  //     pron: 'banan',
  //     freq: 2,
  //     aoa: 4,
  //     type: 'noun',
  //     structure: 'CV-CV-CV'
  //   },
  //   {
  //     _id: '4',
  //     text: 'banane',
  //     pron: 'banan',
  //     freq: 2,
  //     aoa: 4,
  //     type: 'noun',
  //     structure: 'CV-CV-CV'
  //   },
  //   {
  //     _id: '5',
  //     text: 'banane',
  //     pron: 'banan',
  //     freq: 2,
  //     aoa: 4,
  //     type: 'noun',
  //     structure: 'CV-CV-CV'
  //   },
  //   {
  //     _id: '6',
  //     text: 'banane',
  //     pron: 'banan',
  //     freq: 2,
  //     aoa: 4,
  //     type: 'noun',
  //     structure: 'CV-CV-CV'
  //   },
  //   {
  //     _id: '7',
  //     text: 'banane',
  //     pron: 'banan',
  //     freq: 2,
  //     aoa: 4,
  //     type: 'noun',
  //     structure: 'CV-CV-CV'
  //   },
  //   {
  //     _id: '8',
  //     text: 'banane',
  //     pron: 'banan',
  //     freq: 2,
  //     aoa: 4,
  //     type: 'noun',
  //     structure: 'CV-CV-CV'
  //   }
  // ]
  // console.log({ devList })
  return (
    <ul className='word-list'>
      {list.map((item: Word, i) => (
        <WordCard
          key={item._id}
          selected={wordSelection.some(x => x.id === item._id)}
          word={item}
          tag='li'
          index={i}
          onSelected={onWordSelected}
          onUnSelected={onWordUnSelected}
        />
      ))}
    </ul>
  )
}

export default memo(WordList)
