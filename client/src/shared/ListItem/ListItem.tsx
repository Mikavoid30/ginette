import React from 'react'

import './ListItem.scss'

type ListItemProps = {
  children: JSX.Element
}

function ListItem ({ children }: ListItemProps): JSX.Element {
  return <li className='ListItem'>{children}</li>
}

export default ListItem
