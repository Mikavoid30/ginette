import React, { useState, useCallback } from 'react'
import PopHover from '../PopHover/PopHover'
import Title from '../Title/Title'

import './SelectRange.scss'

export type RangeFilterValue = number[]

interface ISelectChangeProps {
  label: string
  name: string
  initialRange: RangeFilterValue
  title?: string
  onChange: (name: string, selectRangeResult: RangeFilterValue) => void
}

function SelectRange ({
  label,
  title = '',
  name = '',
  initialRange = [0, 98],
  onChange
}: ISelectChangeProps): JSX.Element {
  const [isOpen, setIsOpen] = useState(false)
  const [range, setRange] = useState<RangeFilterValue>(initialRange)

  const updateAndClose = () => {
    onChange(name, range)
    setIsOpen(false)
  }

  const getLabel = useCallback(() => {
    if (range[0] === 0 && range[1] === 0) {
      return `Sélectionner ${label.toLowerCase()}`
    }
    return `${range[0]} → ${range[1] === 0 ? '∞' : range[1]}`
  }, [label, range])

  return (
    <div className='select-range'>
      <div className='select-range-label'>{label}</div>
      <div className='select-range-inputs'>
        <div
          className={`select-range-inputs-trigger select-range-inputs-trigger--${
            isOpen ? 'is-open' : ''
          }`}
          onClick={() => {
            setIsOpen(!isOpen)
          }}
        >
          {getLabel()} <span>▼</span>
        </div>
        <PopHover open={isOpen} onClose={updateAndClose}>
          <>
            {title ? <Title level={6}>{title}</Title> : null}
            <form
              onSubmit={e => {
                e.preventDefault()
                updateAndClose()
              }}
            >
              <div className='select-range-inputs-container'>
                <div className='from'>
                  <input
                    type='number'
                    value={range[0]}
                    onChange={e => {
                      const newRange = [...range]
                      newRange[0] = +e.target.value
                      setRange(newRange)
                    }}
                  />
                </div>
                <div className='sep'>→</div>
                <div className='to'>
                  <input
                    type='number'
                    value={range[1]}
                    onChange={e => {
                      const newRange = [...range]
                      newRange[1] = +e.target.value
                      setRange(newRange)
                    }}
                  />
                </div>
              </div>
              <button style={{ display: 'none' }} type='submit' tabIndex={0} />
            </form>
            <span
              className='clear-all'
              onClick={() => {
                setRange([0, 0])
              }}
            >
              Valeurs par défaut
            </span>
          </>
        </PopHover>
      </div>
    </div>
  )
}

export default SelectRange
