import React from 'react'
import { LetterState } from '../../types/keyboard'
import Icon from '../Icons/Icon'
import './Position.scss'

export enum Positions {
  ANY = 0,
  MEDIAN,
  START,
  END
}
type PositionProps = {
  char: string
  letterState: LetterState
  initialPosition?: Positions
  onPositionChanged: (
    char: string,
    position: Positions,
    state: LetterState.Include | LetterState.Exclude | LetterState.None
  ) => any
}

function Position ({
  char,
  letterState,
  initialPosition = Positions.ANY,
  onPositionChanged
}: PositionProps) {
  if (letterState === LetterState.None) return null
  return (
    <div className={`position position--${letterState}`}>
      <div className='position-char'>{char}</div>
      <Icon name={'ArrowRightIcon'} />
      <div className='position-buttons'>
        <button
          type='button'
          className={`position-button${
            initialPosition === Positions.START ? '--is-selected' : ''
          }`}
          onClick={() => onPositionChanged(char, Positions.START, letterState)}
        >
          Début
        </button>
        <button
          type='button'
          className={`position-button${
            initialPosition === Positions.MEDIAN ? '--is-selected' : ''
          }`}
          onClick={() => onPositionChanged(char, Positions.MEDIAN, letterState)}
        >
          Médian
        </button>
        <button
          type='button'
          className={`position-button${
            initialPosition === Positions.END ? '--is-selected' : ''
          }`}
          onClick={() => onPositionChanged(char, Positions.END, letterState)}
        >
          Fin
        </button>
      </div>
    </div>
  )
}

export default Position
