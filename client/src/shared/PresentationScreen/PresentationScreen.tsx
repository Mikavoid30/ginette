import React from 'react'
import { useNavigate } from 'react-router'
import WordCard from '../WordCard/WordCard'

import './PresentationScreen.scss'

type PresentationScreenProps = {}

function PresentationScreen (props: PresentationScreenProps): JSX.Element {
  const navigate = useNavigate()

  const handleSelected = () => {
    navigate('/search')
  }

  return (
    <div className='PresentationScreen'>
      <div className='PresentationScreen_left'>CVC-CVC</div>
      <div className='PresentationScreen_right'>
        <ul className='PresentationScreen_wordList'>
          <WordCard
            key={1}
            selected={false}
            word={{
              _id: '1',
              pron: 'paʁ.tiʁ',
              text: 'partir',
              type: 'ver',
              freq: 788
            }}
            tag='li'
            index={1}
            onSelected={handleSelected}
            onUnSelected={() => {}}
          />
          <WordCard
            key={2}
            selected={false}
            word={{
              _id: '2',
              pron: 'dɔk.tœʁ',
              text: 'docteur',
              type: 'nom',
              aoaMean: 5,
              freq: 223
            }}
            tag='li'
            index={2}
            onSelected={handleSelected}
            onUnSelected={() => {}}
          />
          <WordCard
            key={3}
            selected={false}
            word={{
              _id: '3',
              pron: 'ʃaʁ.mɑ̃t',
              text: 'charmante',
              type: 'adj',
              freq: 21
            }}
            tag='li'
            index={3}
            onSelected={handleSelected}
            onUnSelected={() => {}}
          />
          <WordCard
            key={4}
            selected={false}
            word={{
              _id: '4',
              pron: 'kyl.tyʁ',
              text: 'culture',
              type: 'nom',
              freq: 17
            }}
            tag='li'
            index={4}
            onSelected={handleSelected}
            onUnSelected={() => {}}
          />
        </ul>
      </div>
    </div>
  )
}

export default PresentationScreen
