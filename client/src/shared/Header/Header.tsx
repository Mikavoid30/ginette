import React from 'react'
import Button, { ButtonSize } from '../Button/Button'

import './Header.scss'

type HeaderProps = {
  isLoggedIn: boolean
  avatar: string
  isLoading: boolean
  error: boolean
}

function Header ({
  isLoggedIn,
  isLoading,
  avatar,
  error
}: HeaderProps): JSX.Element {
  return (
    <div className='Header'>
      <div className='Header_container'>
        <div className='Header_logo'>
          <a href={isLoggedIn ? '/search' : '/'}>Phonemia.</a>
        </div>
        {/* 
        {isLoggedIn && (
          <nav className='Header_nav'>
            <ul>
              <li>
                <a href='/search'>Recherche</a>
              </li>
            </ul>
          </nav>
        )} */}

        <div className='Header_user'>
          {!isLoading && (
            <div className='Header_buttons'>
              <ul>
                <li>
                  {isLoggedIn ? (
                    <Button
                      filled
                      size={ButtonSize.MEDIUM}
                      href='/auth/logout'
                      isLink
                    >
                      Se déconnecter
                    </Button>
                  ) : (
                    <Button
                      filled
                      size={ButtonSize.MEDIUM}
                      href='/auth/google'
                      isLink
                    >
                      Se connecter avec Google
                    </Button>
                  )}
                </li>
              </ul>
            </div>
          )}
          {!isLoading && isLoggedIn && avatar && (
            <div className='Header_user_avatar'>
              <img src={avatar} alt='Avatar' />
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default Header
