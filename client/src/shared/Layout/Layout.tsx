import React from 'react'

import './Layout.scss'
import HeaderConnected from '../../containers/HeaderConnected/HeaderConnected'

type LayoutProps = {
  children: JSX.Element | JSX.Element[] | string | null
  verticalMenu?: boolean
}

function Layout ({ children, verticalMenu = false }: LayoutProps): JSX.Element {
  return (
    <div
      className={`Layout ${
        verticalMenu ? 'Layout--hasVerticalMenu' : 'Layout--hasHorizontalMenu'
      }`}
    >
      <HeaderConnected />
      <main>{children}</main>
    </div>
  )
}

export default Layout
