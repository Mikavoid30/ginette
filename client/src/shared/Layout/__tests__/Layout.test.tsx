/* eslint-disable */
import React from 'react'
import { shallow, mount } from 'enzyme'
import Layout from '../Layout'
import Menu from '../../Menu/Menu'
import { MemoryRouter } from 'react-router-dom'

const HORIZONTAL_MENU_CLASS = 'Layout--hasHorizontalMenu'
const VERTICAL_MENU_CLASS = 'Layout--hasVerticalMenu'

test('Snapshot renders Layout', () => {
  const wrapper = shallow(
    <Layout>
      <p>OK</p>
    </Layout>
  )
  expect(wrapper).toMatchSnapshot()
})

test('Should print the component correctly', () => {
  const wrapper = mount(
    <MemoryRouter>
      <Layout>
        <p>OK</p>
      </Layout>
    </MemoryRouter>
  )
  expect(wrapper.find('h1').length).toBe(1)
  expect(wrapper.find('h1').text()).toContain('Layout')
  expect(wrapper.find('p').text()).toContain('OK')
})

test('Should print the menu by default', () => {
  const wrapper = mount(
    <MemoryRouter>
      <Layout>
        <p>OK</p>
      </Layout>
    </MemoryRouter>
  )
  expect(wrapper.find(Menu).length).toBe(1)
  expect(wrapper.find('.Layout').hasClass(HORIZONTAL_MENU_CLASS)).toBe(true)
  expect(wrapper.find('.Layout').hasClass(VERTICAL_MENU_CLASS)).toBe(false)
})

test('Should not print the menu', () => {
  const wrapper = mount(
    <MemoryRouter>
      <Layout verticalMenu>
        <p>OK</p>
      </Layout>
    </MemoryRouter>
  )
  expect(wrapper.find('.Layout').hasClass(VERTICAL_MENU_CLASS)).toBe(true)
  expect(wrapper.find('.Layout').hasClass(HORIZONTAL_MENU_CLASS)).toBe(false)
})
