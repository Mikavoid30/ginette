import React, { memo } from 'react'
import {
  getPublicMediaPath,
  PublicMediaExtensions,
  PublicMediaTypes
} from '../../helpers/images'
import './EmptyPlaceholder.scss'

type EmptyPlaceholderProps = {
  children: string | React.ReactNode | React.ReactNode[]
  illustrationName?: string
}
function EmptyPlaceholder ({
  children,
  illustrationName = 'emptyPlaceholderDefault'
}: EmptyPlaceholderProps) {
  const path = getPublicMediaPath(
    illustrationName,
    PublicMediaTypes.ILLUSTRATIONS,
    PublicMediaExtensions.SVG
  )
  return (
    <div className='empty-placeholder'>
      {children}
      <div className='empty-placeholder-illustration'>
        <img src={path} alt={`illustration`} />
      </div>
    </div>
  )
}

export default memo(EmptyPlaceholder)
