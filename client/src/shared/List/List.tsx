import React from 'react'

import './List.scss'

type ListProps = {
  children: JSX.Element
}

function List ({ children }: ListProps): JSX.Element {
  return (
    <div className='List'>
      <ul>{children}</ul>
    </div>
  )
}

export default List
