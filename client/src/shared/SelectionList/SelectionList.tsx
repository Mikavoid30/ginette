import React from 'react'
import { wordSelectionData } from '../../redux/actions/words/word-selection'
import Button, { ButtonSize } from '../Button/Button'

import './SelectionList.scss'

type SelectionListProps = {
  list: any
  onSaveList: (idList: string[]) => any
  onResetSelection: () => any
  onDownloadPdfFromList: (idList: string[]) => any
  onRemoveWordFromSelection: (word: wordSelectionData) => any
}

function SectionItem ({
  id,
  text,
  pron,
  index,
  onRemoveWordFromSelection
}: {
  id: string
  text: string
  pron: string
  index: number
  onRemoveWordFromSelection: (word: wordSelectionData) => any
}) {
  return (
    <li className='selection-list-item'>
      <p>
        <span className='index'>#{index + 1}</span>
        <span className='text'>{text}</span>
        <span className='pron'>/{pron}/</span>
      </p>
      <button
        className='remove'
        onClick={() => {
          onRemoveWordFromSelection({ id, text, pron })
        }}
      >
        +
      </button>
    </li>
  )
}

function SelectionList ({
  list,
  onSaveList,
  onResetSelection,
  onDownloadPdfFromList,
  onRemoveWordFromSelection
}: SelectionListProps): JSX.Element {
  const emptyList = list?.length <= 0

  const clearButton = () => {
    return (
      <a
        className='clear-button'
        href='#'
        onClick={e => {
          e.preventDefault()
          onResetSelection()
        }}
      >
        Effacer la sélection {'(' + list.length + ')'}
      </a>
    )
  }
  return (
    <div className='selection-list'>
      <h1>SELECTION </h1>

      {emptyList ? (
        <p>Aucune sélection</p>
      ) : (
        <>
          {clearButton()}
          <ul className='list'>
            {list.map((x: any, i: number) => {
              return (
                <SectionItem
                  key={x.text}
                  text={x.text}
                  pron={x.pron}
                  id={x.id}
                  onRemoveWordFromSelection={onRemoveWordFromSelection}
                  index={i}
                />
              )
            })}
          </ul>

          <br />
          <Button
            size={ButtonSize.MAXI}
            primary
            onClick={() => {
              onSaveList(list.map((x: any) => x.id))
            }}
          >
            Save
          </Button>
          <Button
            size={ButtonSize.MAXI}
            primary
            onClick={() => {
              onDownloadPdfFromList(list.map((x: any) => x.id))
            }}
          >
            Export PDF
          </Button>
        </>
      )}
    </div>
  )
}

export default SelectionList
