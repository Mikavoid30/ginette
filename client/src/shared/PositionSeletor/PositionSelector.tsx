import React from 'react'
import { LetterState } from '../../types/keyboard'
import { KeyboardState } from '../Keyboard/Keyboard'
import Position, { Positions } from '../Position/Position'
import Title from '../Title/Title'

import './PositionSelector.scss'

type PositionSelectorProps = {
  keys: KeyboardState
  onPositionChanged: (char: string, position: Positions) => any
}
function PositionSelector ({ keys, onPositionChanged }: PositionSelectorProps) {
  const handlePositionChanged = (char: string, position: Positions) => {
    onPositionChanged(char, position)
  }
  const renderOneKey = (key: string, i: number) => {
    return (
      <Position
        key={key + i}
        char={key}
        letterState={keys[key]?.state || LetterState.Include}
        initialPosition={keys[key].position || Positions.MEDIAN}
        onPositionChanged={handlePositionChanged}
      />
    )
  }
  const ks = Object.keys(keys)
  const visible = ks && ks.length
  return (
    <div
      className={`position-selector position-selector--${
        visible ? 'visible' : 'invisible'
      }`}
    >
      {visible ? <>{Object.keys(keys).map(renderOneKey)}</> : null}
    </div>
  )
}

export default PositionSelector
