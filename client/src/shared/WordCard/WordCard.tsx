import React from 'react'
import { wordSelectionData } from '../../redux/actions/words/word-selection'
import './WordCard.scss'

type WordCardProps = {
  index: number
  tag: 'div' | 'li' | 'p'
  selected: boolean
  word: {
    _id: string
    text: string
    pron: string
    type?: string
    aoaMean?: number
    freq?: number
  }
  onSelected: (data: wordSelectionData) => any
  onUnSelected: (data: wordSelectionData) => any
}
function WordCard ({
  tag = 'li',
  index = 0,
  selected = false,
  onSelected,
  onUnSelected,
  word: { _id, text, pron, type = 'noun', aoaMean: aoa = 1, freq = 1 }
}: WordCardProps) {
  const Tag = tag
  return (
    <Tag
      className={`word-card ${selected ? 'word-card--selected' : ''}`}
      onClick={() => {
        const data: wordSelectionData = {
          text,
          id: _id,
          pron
        }
        if (selected) {
          return onUnSelected(data)
        }
        onSelected(data)
      }}
      style={{ animationDelay: `${index * 10}ms` }}
    >
      <div className='word-card-container'>
        <div className='word-card-container-header'>
          <div className='freq'>
            {freq !== null ? <span>f: {freq}</span> : <span />}
            {aoa !== null ? <span>aoa: {aoa.toPrecision(2)}</span> : <span />}
          </div>
          <div className={`type type--${type.split(':')[0].toLowerCase()}`}>
            {type}
          </div>
        </div>
        <div className='word-card-container-body'>
          <div className='text'>{text}</div>

          <div className='pron'>{pron}</div>
        </div>
      </div>
    </Tag>
  )
}

export default WordCard
