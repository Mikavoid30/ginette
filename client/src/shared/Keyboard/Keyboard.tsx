import React, { memo, useState } from 'react'

import './Keyboard.scss'
import { Letter, LetterState } from '../../types/keyboard'
import { arrayChunk } from '../../helpers/utils'
import { Positions } from '../Position/Position'
import { CONSONANTS, VOWELS } from '../../constants'

const letters = [CONSONANTS, VOWELS]

type KeyData = {
  state?: LetterState
  position?: Positions
}

type KeyboardProps = {
  letters?: string[][]
  titles?: string[]
  nbLettersByLine?: number
  keys: KeyboardState
  onChange: (char: string, state: LetterState) => any
}

export type KeyboardState = {
  [s: string]: KeyData
}
function generateLettersFromSimpleArray (arr: string[][]): Letter[][] {
  return arr.map(line => {
    return line?.map(c => ({
      char: c,
      state: LetterState.None
    }))
  })
}

function getLetterStateClassName (
  letterState: LetterState,
  prefix: string = 'Letter'
) {
  switch (letterState) {
    case LetterState.None:
      return `${prefix}--none-state`
    case LetterState.Include:
      return `${prefix}--include-state`
    case LetterState.Exclude:
      return `${prefix}--exclude-state`
    default:
      return ''
  }
}

function Keyboard ({
  keys,
  nbLettersByLine = 7,
  onChange
}: KeyboardProps): JSX.Element {
  const convertedLetters = generateLettersFromSimpleArray(letters)
  // const [keys, setKeys] = useState(initialKeys)

  const keyClicked = (letter: Letter) => {
    let newState = 1
    if (keys[letter.char]) {
      newState = ((keys[letter?.char]?.state || LetterState.Include) + 1) % 3
    }
    onChange(letter?.char, newState)
  }

  return (
    <div className='Keyboard'>
      <section className='Keyboard-keys'>
        {convertedLetters.map((letters, i) => {
          return (
            <div className='Keyboard-keys-row' key={'k' + i}>
              {/* {titles[i] ? <Title level={5}>{titles[i]}</Title> : null} */}
              {arrayChunk<Letter>(letters, nbLettersByLine).map(
                (letters, rowIdx) => {
                  return (
                    <div className='Keyboard-keys-line' key={rowIdx}>
                      {letters.map((letter, idx) => (
                        <button
                          key={letter.char + i + idx + rowIdx}
                          className={`Keyboard-keys-key ${getLetterStateClassName(
                            keys[letter.char]?.state || LetterState.None,
                            'Keyboard-keys-key'
                          )}`}
                          onClick={() => keyClicked(letter)}
                          type='button'
                        >
                          {letter.char}
                        </button>
                      ))}
                    </div>
                  )
                }
              )}
            </div>
          )
        })}
      </section>
    </div>
  )
}

export default memo(Keyboard)
