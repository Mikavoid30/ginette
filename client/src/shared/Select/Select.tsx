import React, { useState, memo, useCallback } from 'react'
import PopHover from '../PopHover/PopHover'
import Title from '../Title/Title'

import './Select.scss'

export type TextFilterValue = {
  text: string
  checked?: boolean
}

interface ISelectProps {
  multiple?: boolean
  options: TextFilterValue[]
  label: string
  name: string
  title?: string
  onChange: (name: string, values: TextFilterValue[]) => any
}

function Select ({
  label,
  name,
  options,
  onChange = w => null,
  multiple = false,
  title = ''
}: ISelectProps): JSX.Element {
  const [isOpen, setIsOpen] = useState(false)
  const [values, setValues] = useState(options)

  const updateAndClose = useCallback(() => {
    onChange(name, values)
    setIsOpen(false)
  }, [values, name, onChange])

  const getLabel = useCallback(
    (initialLabel = '') => {
      const nbChecked = values
        .map(x => x)
        .reduce((p, c) => (c.checked ? p + 1 : p), 0)
      if (nbChecked === values.length) return 'Tout'
      if (nbChecked === 0) return initialLabel

      return values
        .map(x => (x.checked ? x.text : null))
        .filter(x => x != null)
        .join(', ')
    },
    [values]
  )

  const optionClicked = (e: any, value: string) => {
    const newValues = values.map(v => {
      if (v.text === value) {
        return { ...v, checked: !v.checked }
      }
      return v
    })
    setValues(newValues)

    if (!multiple) {
      onChange(name, newValues)
      setIsOpen(false)
    }
  }
  return (
    <div className='select'>
      <div className='select-label'>{label}</div>
      <div className='select-options'>
        <div
          className={`select-options-trigger select-options-trigger--${
            isOpen ? 'is-open' : ''
          }`}
          onClick={() => {
            setIsOpen(!isOpen)
          }}
        >
          {getLabel('Sélectionner ' + label.toLowerCase())} <span>▼</span>
        </div>
        <PopHover open={isOpen} onClose={updateAndClose}>
          <div
            className={`select-options-container select-options-container--${
              isOpen ? 'is-open' : 'is-closed'
            }`}
          >
            {title ? <Title level={6}>{title}</Title> : null}
            <ul className='select-options-container-list'>
              {values.map((opt, i) => (
                <li
                  key={opt.text + i}
                  className={`option option--${
                    opt.checked ? 'is-checked' : ''
                  }`}
                  onClick={e => optionClicked(e, opt.text)}
                >
                  <span>{opt.checked ? '✓' : ''}</span>
                  {opt.text}
                </li>
              ))}
            </ul>
            <span
              className='clear-all'
              onClick={() => {
                const shouldUnCheckAll = values.some(x => x.checked)
                setValues(
                  values.map(v => ({ ...v, checked: !shouldUnCheckAll }))
                )
              }}
            >
              {values.some(x => x.checked)
                ? 'Tout déseclectionner'
                : 'Tout sélectionner'}
            </span>
          </div>
        </PopHover>
      </div>
    </div>
  )
}

export default memo(Select)
