import React from 'react'

import './SaveNewItem.scss'

type SaveNewItem = {}

function SaveNewItem (props: SaveNewItem): JSX.Element {
  return (
    <div className='SaveNewitem'>
      <div className='slide-container-body-word'>
        <div className='text'>Test</div>
        <div className='pron'>Test</div>
      </div>
    </div>
  )
}

export default SaveNewItem
