import React from 'react'
import { IModal } from '../../Modal'

import './GenericModal.scss'

function GenericModal ({ open, children }: IModal): JSX.Element | null {
  if (!open) return null
  return (
    <div className={`generic-modal ${open ? 'generic-modal--is-open' : ''}`}>
      {children}
    </div>
  )
}

export default GenericModal
