import React from 'react'
import LoadingSpinner from '../../../../LoadingSpinner/LoadingSpinner'

import './Slide.scss'

type SlideProps = {
  loading: boolean
  wordDetails?: {
    word: {
      _id: string
      pron: string
      text: string
    }
    images: any[]
  }
}

function Slide ({
  loading,
  wordDetails = {
    images: [],
    word: {
      _id: '0',
      pron: '',
      text: ''
    }
  }
}: SlideProps): JSX.Element {
  const image = wordDetails?.images?.length
    ? wordDetails.images[0].webformatURL
    : null
  return (
    <div className={`slide`}>
      <div className='slide-container-body-word'>
        <div className='text'>{wordDetails.word.text}</div>
        <div className='pron'>{wordDetails.word.pron}</div>
      </div>
      {image !== null ? (
        <div className='slide-container-body-images'>
          <div className='main-image-container'>
            {loading ? (
              <LoadingSpinner />
            ) : (
              <>
                <img src={image} alt='' />
              </>
            )}
          </div>
        </div>
      ) : null}
    </div>
  )
}

export default Slide
