import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { fetchWordDetails } from '../../../../redux/actions/words/word'
import { wordSelectionData } from '../../../../redux/actions/words/word-selection'
import Button, { ButtonSize } from '../../../Button/Button'
import { KEY_ARROW_RIGHT, KEY_ARROW_LEFT } from '../../../../constants'

import './Diaporama.scss'
import Slide from './Slide/Slide'

type DiaporamaProps = {}

function Diaporama (): JSX.Element {
  const dispatch = useDispatch()
  const [currentSlideId, setCurrentSlideId] = useState(0)
  const wordSelection = useSelector((state: any) => state.words.selection)
  const wordDetails = useSelector((state: any) => state.words.wordDetails)
  const wordDetailsLoading = useSelector(
    (state: any) => state.words.wordDetails.loading
  )

  const goToNextSlide = () => {
    setCurrentSlideId((currentSlideId + 1) % wordSelection.length)
  }

  const goToPreviousSlide = () => {
    if (currentSlideId === 0) {
      return setCurrentSlideId(wordSelection.length - 1)
    }
    setCurrentSlideId((currentSlideId - 1) % wordSelection.length)
  }

  const onKeyDown = (k: any) => {
    switch (k.key) {
      case KEY_ARROW_RIGHT:
        return goToNextSlide()
      case KEY_ARROW_LEFT:
        return goToPreviousSlide()
      default:
        return null
    }
  }

  useEffect(() => {
    dispatch(fetchWordDetails(wordSelection[currentSlideId].id))
  }, [currentSlideId, dispatch])

  useEffect(() => {
    window.addEventListener('keydown', onKeyDown)
    return () => {
      window.removeEventListener('keydown', onKeyDown)
    }
  }, [onKeyDown])

  const renderSlide = (
    words: wordSelectionData[],
    currentSlideId: number = 0
  ) => {
    const w = words[currentSlideId]
    if (!w) return null
    return (
      <Slide
        key={w.id}
        wordDetails={wordDetails}
        loading={wordDetailsLoading}
      />
    )
  }
  return (
    <div className={`diaporama`}>
      <div className='diaporama-container'>
        {renderSlide(wordSelection, currentSlideId)}
        <div className='diaporama-container-footer'>
          <Button size={ButtonSize.MEDIUM} onClick={goToPreviousSlide}>
            Précédent
          </Button>
          <Button size={ButtonSize.MEDIUM} primary onClick={goToNextSlide}>
            Suivant
          </Button>
        </div>
      </div>
    </div>
  )
}

export default Diaporama
