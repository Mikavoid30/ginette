import React, { useEffect } from 'react'
import ReactDOM from 'react-dom'
import { useSelector, useDispatch } from 'react-redux'
import Diaporama from './modals/Diaporama/Diaporama'
import GenericModal from './modals/GenericModal/GenericModal'
import { GenericModalTypes } from '../../redux/reducers/modal'
import { closeModal } from '../../redux/actions/modal'
import { KEY_ESCAPE } from '../../constants'
import './Modal.scss'

export interface IModal {
  open: boolean
  children?: JSX.Element | string
}
type ModalProps = {}

function renderSpecificModal (modalType: GenericModalTypes) {
  switch (modalType) {
    case GenericModalTypes.DIAPORAMA:
      return <Diaporama />
    default:
      return <div></div>
  }
}

function Modal (props: ModalProps) {
  const dispatch = useDispatch()
  const open = useSelector((state: any) => state.modal.open)
  const modalType = useSelector((state: any) => state.modal.modalType)

  const close = () => {
    dispatch(closeModal(modalType))
  }

  const onKeyDown = (k: any) => {
    if (k.key === KEY_ESCAPE) {
      return close()
    }
  }

  useEffect(() => {
    window.addEventListener('keydown', onKeyDown)

    return () => {
      window.removeEventListener('keydown', onKeyDown)
    }
  }, [onKeyDown])

  return ReactDOM.createPortal(
    <GenericModal open={open}>
      <div className='modal'>
        <button className='modal-close-button' onClick={close}>
          +
        </button>
        {renderSpecificModal(modalType)}
      </div>
    </GenericModal>,
    document.getElementById('portal') as Element
  )
}

export default Modal
