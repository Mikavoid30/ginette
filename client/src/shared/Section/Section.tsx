import React from 'react'

import './Section.scss'

type SectionProps = {
  children: JSX.Element
}

function Section ({ children }: SectionProps): JSX.Element {
  return (
    <section className='Section'>
      <h1>Section</h1>
      {children}
    </section>
  )
}

export default Section
