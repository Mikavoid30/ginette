import React from 'react'
import './Legend.scss'

type Legend = {
  text: string
  color: string
}

type LegendProps = {
  items: Legend[]
  invisible?: boolean
}

function Legend ({ items, invisible = false }: LegendProps) {
  if (invisible) return null
  const renderOneLegend = (item: Legend) => {
    return (
      <li className='legend-item' key={item.text} style={{ color: item.color }}>
        <div
          className='legend-item-bullet'
          style={{ backgroundColor: item.color }}
        />
        <p>{item.text}</p>
      </li>
    )
  }
  return (
    <div className='legend'>
      <ul>{items.map(renderOneLegend)}</ul>
    </div>
  )
}

export default Legend
