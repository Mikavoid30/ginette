import React, { useEffect } from 'react'
import { formatPositionsToKeys } from '../../helpers/search'
import List from '../List/List'
import ListItem from '../ListItem/ListItem'
import Title from '../Title/Title'

import './SearchHistory.scss'

type SearchHistoryProps = {
  history: any
  navigate: any
  clearSearchesHistory: () => void
  getLatestSearches: (nb: number) => any
  initSearchForm: (state: any) => any
  submitSearchForm: (doNotSaveInHistory: boolean) => any
}

function SearchHistory ({
  history,
  clearSearchesHistory = () => {},
  getLatestSearches = () => {},
  initSearchForm = () => {},
  submitSearchForm = () => {}
}: SearchHistoryProps): JSX.Element {
  useEffect(() => {
    getLatestSearches(50)
  }, [])
  return (
    <div className='SearchHistory'>
      <div className='SearchHistory-container'>
        <div className='SearchHistory-heading'>
          <div className='SearchHistory-heading-title'>
            <Title level={5}>Historique de recherche</Title>
          </div>
          <div className='links'>
            <a href='#' onClick={() => clearSearchesHistory()}>
              vider
            </a>
          </div>
        </div>
        <List>
          <>
            {history?.length ? (
              history.map((h: any) => (
                <ListItem key={h._id}>
                  <>
                    <p className='date'>
                      {new Date(h.createdAt).toLocaleString()}
                    </p>
                    <p>
                      <strong>{h.structure || 'Strucutre non définie'}</strong>
                    </p>
                    <p>
                      <span className='inclusions' title='Inclusions'>
                        [
                        <b title='Inclusions au début du mot'>
                          {h?.inclusions.init.join(',')}
                        </b>
                        ][
                        <b title='Inclusions au milieu du mot'>
                          {h?.inclusions.median.join(',')}
                        </b>
                        ][
                        <b title='Inclusions à la fin du mot'>
                          {h?.inclusions.end.join(',')}
                        </b>
                        ]
                      </span>{' '}
                    </p>
                    <p>
                      <span className='exclusions' title='Exclusions'>
                        [
                        <b title='Exclusions au début du mot'>
                          {h?.exclusions.init.join(',')}
                        </b>
                        ][
                        <b title='Exclusions au milieu du mot'>
                          {h?.exclusions.median.join(',')}
                        </b>
                        ][
                        <b title='Exclusions au fin du mot'>
                          {h?.exclusions.end.join(',')}
                        </b>
                        ]
                      </span>
                    </p>
                    <button
                      className='searchButton'
                      onClick={() => {
                        initSearchForm({
                          structure: h?.structure,
                          inclusions: h?.inclusions,
                          exclusions: h?.exclusions,
                          filters: h?.filters,
                          keys: formatPositionsToKeys(
                            h?.inclusions,
                            h?.exclusions
                          )
                        })
                        submitSearchForm(true)
                      }}
                    >
                      ►
                    </button>
                  </>
                </ListItem>
              ))
            ) : (
              <p>Aucun historique</p>
            )}
          </>
        </List>
      </div>
    </div>
  )
}

export default SearchHistory
