import React from 'react'

import './PopHover.scss'

type PopHoverProps = {
  children: JSX.Element
  open?: boolean
  onClose: () => any
}

function PopHover ({
  children,
  open = false,
  onClose
}: PopHoverProps): JSX.Element {
  return (
    <div className={`pophover`}>
      {open ? <div className='pophover-dimmer' onClick={onClose}></div> : null}
      <div
        className={`pophover-container pophover-container--${
          open ? 'is-open' : 'is-closed'
        }`}
      >
        {children}
      </div>
    </div>
  )
}

export default PopHover
