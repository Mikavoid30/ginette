/* eslint-disable */
import React from 'react'
import { shallow } from 'enzyme'
import Button from '../Button'

test('Snapshot renders Button.test', () => {
  const wrapper = shallow(<Button />)
  expect(wrapper).toMatchSnapshot()
})
