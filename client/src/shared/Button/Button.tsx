import React from 'react'
import classnames from 'classnames'

import './Button.scss'

export enum ButtonSize {
  MINI = 'mini',
  MEDIUM = 'medium',
  BIG = 'big',
  MAXI = 'maxi'
}

type ButtonProps = {
  children?: JSX.Element | JSX.Element[] | string | null
  size: ButtonSize
  type?: 'submit' | 'button'
  primary?: boolean
  secondary?: boolean
  disabled?: boolean
  filled?: boolean
  third?: boolean
  white?: boolean
  isLink?: boolean
  href?: string
  onClick?: (x: any) => any
}

function Button ({
  children,
  type = 'button',
  size = ButtonSize.MINI,
  disabled = false,
  filled = false,
  onClick = () => null,
  primary = false,
  secondary = false,
  isLink = false,
  white = false,
  third = false,
  href = '#'
}: ButtonProps): JSX.Element {
  const Tag = isLink ? 'a' : 'button'
  return (
    <Tag
      disabled={disabled}
      type={type}
      className={classnames('Button', `Button--${size}`, {
        'Button--primary': primary,
        'Button--secondary': secondary,
        'Button--third': third,
        'Button--white': white,
        'Button--filled': filled
      })}
      onClick={onClick}
      href={href}
    >
      {children}
    </Tag>
  )
}

export default Button
