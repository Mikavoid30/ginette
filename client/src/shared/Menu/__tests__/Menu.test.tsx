/* eslint-disable */
/* tsc-disable */
import React from 'react'
import { shallow, mount } from 'enzyme'
import Menu from '../Menu'

import { MemoryRouter } from 'react-router-dom'
import Icon from '../../Icons/Icon'

const ACTIVE_CLASS = 'Menu-item--isActive'
const VERTICAL_CLASS = 'Menu--vertical'
const HORIZONTAL_CLASS = 'Menu--horizontal'

test('Snapshot renders Menu', () => {
  const wrapper: any = shallow(
    <MemoryRouter initialEntries={['/random']}>
      <Menu />
    </MemoryRouter>
  )
  expect(wrapper).toMatchSnapshot()
})

test('Should print the component correctly', () => {
  const wrapper = mount(
    <MemoryRouter initialEntries={['/results']}>
      <Menu />
    </MemoryRouter>
  )
  expect(wrapper.find('nav').length).toBe(1)
  expect(wrapper.find(Icon).length).toBe(3)
})

test('Should hav a horizontal class by default', () => {
  const wrapper = mount(
    <MemoryRouter>
      <Menu vertical />
    </MemoryRouter>
  )
  expect(wrapper.find('.Menu').hasClass(VERTICAL_CLASS)).toBe(true)
  expect(wrapper.find('.Menu').hasClass(HORIZONTAL_CLASS)).toBe(false)
})

test('Should hav a vertical class nav', () => {
  const wrapper = mount(
    <MemoryRouter>
      <Menu />
    </MemoryRouter>
  )
  expect(wrapper.find('.Menu').hasClass(HORIZONTAL_CLASS)).toBe(true)
  expect(wrapper.find('.Menu').hasClass(VERTICAL_CLASS)).toBe(false)
})
