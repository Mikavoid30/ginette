import React from 'react'
import { NavLink } from 'react-router-dom'
import { HomeIcon, SearchIcon } from '../Icons'
import Logo from '../Logo/Logo'
import './Menu.scss'

type MenuProps = {
  vertical?: boolean
}

function Menu ({ vertical = false }: MenuProps): JSX.Element {
  return (
    <div className={`Menu Menu--${vertical ? 'vertical' : 'horizontal'}`}>
      <div className='Menu-logo'>
        <Logo />
      </div>
      <nav>
        <ul className='Menu-items'>
          <li>
            <NavLink to='/dashboard' className='Menu-item'>
              <HomeIcon />
            </NavLink>
          </li>
          <li>
            <NavLink to='search' className='Menu-item'>
              <SearchIcon />
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  )
}

export default Menu
