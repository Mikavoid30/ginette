import React, { memo } from 'react'
import { FILTER_FIELDS, WORD_TYPES } from '../../constants'
import Select, { TextFilterValue } from '../Select/Select'
import SelectRange, { RangeFilterValue } from '../SelectRange/SelectRange'
import { useSelector, useDispatch } from 'react-redux'
import './SearchToolbar.scss'
import {
  submitSearchForm,
  updateSearchRangesFilters,
  updateSearchTextFilters
} from '../../redux/actions/search'
import classNames from 'classnames'

type SearchToolbarProps = {
  wordSelection: string[]
  contentsLength: number
  firstEntry?: boolean
  onDiaporamaClicked: () => any
  onResetSelection: () => any
}

function SearchToolbar ({ firstEntry }: SearchToolbarProps): JSX.Element {
  const dispatch = useDispatch()
  const initialFilters = useSelector((state: any) => state.search.form.filters)

  const onRangeFilterChanged = (name: string, range: RangeFilterValue) => {
    dispatch(updateSearchRangesFilters({ [name]: range }))
    dispatch(submitSearchForm())
  }

  const onTextFilterChanged = async (name: string, values: any) => {
    const checked = (values as TextFilterValue[]).filter(v => v.checked)
    const fs = checked?.length
      ? {
          [name]: checked.map(x => x.text + '*')
        }
      : {}
    dispatch(updateSearchTextFilters(fs))
    dispatch(submitSearchForm())
  }

  const initialTextTypeValues = Object.keys(WORD_TYPES).map(x => ({
    text: WORD_TYPES[x],
    checked: !!initialFilters?.text?.type?.some((checked: any) =>
      checked.startsWith(x)
    )
  }))

  return (
    <div
      className={classNames('SearchToolbar', {
        'SearchToolbar--closed': firstEntry
      })}
    >
      <div className='SearchToolbar_head'>
        {!firstEntry ? (
          <>
            <Select
              multiple
              onChange={onTextFilterChanged}
              options={initialTextTypeValues}
              label={FILTER_FIELDS.TYPE.label}
              name={FILTER_FIELDS.TYPE.name}
              title={FILTER_FIELDS.TYPE.title}
            />
            <SelectRange
              onChange={onRangeFilterChanged}
              initialRange={
                initialFilters?.ranges?.aoaMean ||
                FILTER_FIELDS.AOA.initialValue
              }
              name={FILTER_FIELDS.AOA.name}
              label={FILTER_FIELDS.AOA.label}
              title={FILTER_FIELDS.AOA.title}
            />
            <SelectRange
              onChange={onRangeFilterChanged}
              initialRange={
                initialFilters?.ranges?.freq || FILTER_FIELDS.FREQ.initialValue
              }
              name={FILTER_FIELDS.FREQ.name}
              label={FILTER_FIELDS.FREQ.label}
              title={FILTER_FIELDS.FREQ.title}
            />
          </>
        ) : null}
      </div>
      <div className='SearchToolbar_footer'>
        {/* {wordSelection?.length ? (
          <>
            <Button size={ButtonSize.MINI} onClick={onResetSelection}>
              Tout déselectionner
            </Button>
            <Button
              size={ButtonSize.MINI}
              disabled={disabled}
              primary
              onClick={onDiaporamaClicked}
            >
              {`Diaporama (${wordSelection.length})`}
            </Button>
          </>
        ) : null} */}
      </div>
    </div>
  )
}

export default memo(SearchToolbar)
