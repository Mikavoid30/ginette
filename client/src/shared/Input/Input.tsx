import React from 'react'
import classnames from 'classnames'
import Icon from '../Icons/Icon'

import './Input.scss'

type InputProps = {
  placeholder: string
  icon: string
  fluid?: boolean
  type?: 'text' | 'password'
  value?: string
  onChange?: (x: any) => any
  error?: string
  legend?: string
}
function Input ({
  placeholder,
  icon,
  value,
  type = 'text',
  error = 'error',
  fluid = false,
  legend = '',
  onChange = () => null
}: InputProps) {
  return (
    <div className='Input'>
      <div
        className={classnames('Input_container', {
          'Input_container--is-fluid': fluid
        })}
      >
        <input
          type={type}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
        />
        <div className='Input_icon'>
          <Icon name={icon} />
        </div>
      </div>
      {legend && (
        <div className='Input_legend'>
          <p>{legend}</p>
        </div>
      )}
      {error && (
        <div className='Input_error'>{error ? <p>{error}</p> : null}</div>
      )}
    </div>
  )
}

export default Input
