type IconProps = {
  name?: string
  inverted?: boolean
}
