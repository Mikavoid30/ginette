import React from 'react'
import {
  getPublicMediaPath,
  PublicMediaExtensions,
  PublicMediaTypes
} from '../../helpers/images'

function Icon ({ name = '', inverted = false }: IconProps): JSX.Element {
  const path = getPublicMediaPath(
    name,
    PublicMediaTypes.ICONS,
    PublicMediaExtensions.SVG
  )

  return (
    <div className={`Icon Icon--${inverted ? 'inverted' : 'regular'}`}>
      <img src={path} alt={`icon ${name}`} />
    </div>
  )
}

export default Icon
