import React from 'react'
import Icon from '../Icon'

function SearchIcon (props: IconProps): JSX.Element {
  return <Icon {...props} name={props.name || 'SearchIcon'} />
}

export default SearchIcon
