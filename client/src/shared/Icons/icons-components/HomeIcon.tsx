import React from 'react'
import Icon from '../Icon'

function HomeIcon (props: IconProps): JSX.Element {
  return <Icon {...props} name={props.name || 'HomeIcon'} />
}

export default HomeIcon
