import React from 'react'
import Icon from '../Icon'

function UsersIcon (props: IconProps): JSX.Element {
  return <Icon {...props} name={props.name || 'UsersIcon'} />
}

export default UsersIcon
