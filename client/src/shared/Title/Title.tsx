import React from 'react'
import './Title.scss'

type TitleProp = {
  children?: React.ReactNode
  level?: 1 | 2 | 3 | 4 | 5 | 6
}
function Title ({ children, level = 6 }: TitleProp) {
  const Tag: any = `h${level}`
  return (
    <div className='title'>
      <Tag>{children}</Tag>
    </div>
  )
}

export default Title
