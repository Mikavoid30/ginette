import React, { useState, memo, useEffect, useMemo } from 'react'
import classnames from 'classnames'
import { LetterState } from '../../types/keyboard'
import Keyboard, { KeyboardState } from '../Keyboard/Keyboard'
import PositionSelector from '../PositionSeletor/PositionSelector'
import { useDispatch, useSelector } from 'react-redux'
import {
  INCLUSION_COLOR,
  EXCLUSION_COLOR,
  VALID_STUCTURE_CHARACTERS
} from '../../constants'
import { Positions } from '../Position/Position'
import Input from '../Input/Input'
import Legend from '../Legend/Legend'
import Button, { ButtonSize } from '../Button/Button'

import './SearchForm.scss'
import { useLocation } from 'react-router'
import {
  initSearchForm,
  newKeyPosition,
  newKeyState,
  resetSearchForm,
  submitSearchForm,
  updateSearchForm
} from '../../redux/actions/search'
import { formatPositionsToKeys } from '../../helpers/search'

type SearchFormProps = {}

function SearchForm (props: SearchFormProps) {
  const { state = {} } = useLocation()
  const dispatch = useDispatch()
  const searchForm = useSelector((state: any) => state.search.form)
  const keys = useSelector((state: any) => state.search.form.keys)
  const locationState = state as any
  let initialSearch: KeyboardState = locationState as KeyboardState

  const positionIsOpen = useMemo(() => {
    return !!Object.keys(keys)?.length
  }, [keys])

  useEffect(() => {
    if (initialSearch && Object.keys(initialSearch).length > 0) {
      dispatch(
        initSearchForm({
          structure: initialSearch.structure,
          filters: initialSearch.filters,
          keys: formatPositionsToKeys(
            initialSearch.inclusions,
            initialSearch.exclusions
          )
        })
      )
      dispatch(submitSearchForm(true))
    }

    return () => {
      dispatch(resetSearchForm())
    }
  }, [])

  const [structureError, setStructureError] = useState<string>('')
  const resetState = () => {
    dispatch(resetSearchForm())
  }

  const handleKeyboardChange = (char: string, state: LetterState) => {
    dispatch(newKeyState({ char, state }))
  }

  const handleSubmit = (e: any) => {
    e.preventDefault()
    dispatch(submitSearchForm())
  }

  return (
    <form
      className={classnames('SearchForm', {
        'SearchForm--position-is-open': positionIsOpen
      })}
      onSubmit={handleSubmit}
    >
      <div className='SearchForm_keyboard-container'>
        <div className='SearchForm-structure-input'>
          <Input
            icon='SearchIcon'
            legend='exemple: CVC-CCV'
            fluid
            placeholder='Structure'
            value={searchForm.structure}
            onChange={e => {
              const lastChar = e.target?.value.slice(-1)
              if (
                !e.target.value ||
                VALID_STUCTURE_CHARACTERS.includes(lastChar.toUpperCase())
              ) {
                if (structureError) setStructureError('')
                dispatch(
                  updateSearchForm({ structure: e.target.value.toUpperCase() })
                )
              } else {
                setStructureError(
                  `Vous devez taper un de ces caractères: "${VALID_STUCTURE_CHARACTERS.join(
                    ', '
                  )}"`
                )
              }
            }}
            error={structureError}
          />
        </div>
        <Legend
          invisible={!Object.keys(keys)?.length}
          items={[
            { color: INCLUSION_COLOR, text: 'Inclusion' },
            { color: EXCLUSION_COLOR, text: 'Exclusion' }
          ]}
        />

        <Keyboard onChange={handleKeyboardChange} keys={keys} />
      </div>
      <div className='SearchForm_positions'>
        <PositionSelector
          keys={keys}
          onPositionChanged={(char: string, position: Positions) => {
            dispatch(newKeyPosition({ char, position }))
          }}
        />
      </div>

      <div className='SearchForm_buttons'>
        <Button size={ButtonSize.MINI} white onClick={resetState}>
          remettre à zéro
        </Button>
        <Button size={ButtonSize.BIG} white filled type='submit'>
          rechercher
        </Button>
      </div>
    </form>
  )
}

export default memo(SearchForm)
