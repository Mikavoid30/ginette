import axios from 'axios'
import React, { useState, memo } from 'react'
import { TWITTER } from '../../constants'

import './Help.scss'

type HelpProps = {}

function Help (props: HelpProps): JSX.Element {
  const [open, setOpen] = useState(false)
  return (
    <div className='help'>
      <a
        href='/'
        onClick={e => {
          e.preventDefault()
          // setOpen(!open)

          // axios
          //   .post('/api/v1/group', {
          //     displayName: 'Super groupe',
          //     description: 'aucune desc'
          //   })
          //   .then(res => {
          //     console.log('SUCCESS', res.data)
          //   })
          //   .catch(e => {
          //     console.log('FAILED', e)
          //   })

          // axios
          //   .get('/api/v1/group')
          //   .then(res => {
          //     console.log('SUCCESS', res.data)
          //   })
          //   .catch(e => {
          //     console.log('FAILED', e)
          //   })
          axios
            .get('/api/v1/list')
            .then(res => {
              console.log('SUCCESS', res.data)
            })
            .catch(e => {
              console.log('FAILED', e)
            })
        }}
      >
        J'ai besoin d'aide
      </a>
      <div className={`help-text help-text--${open ? 'is-open' : 'is-closed'}`}>
        <ul>
          <li>
            Inscrivez une structure syntaxique dans le champ dénommé
            "STRUCTURE". <br />
            Vous pouvez séparer les syllabes par des tirets "-". Exemple:{' '}
            <em>CVC-CVC</em>.
          </li>
          <li>
            Vous pouvez sur le clavier phonétique, sélectionner un phonème. En
            cliquant une fois dessus, celui-ci devient bleu. Celà signifie que
            vous voulez inclure ce phonème dans la recherche. Si vous cliquez
            une deuxième fois, celui-ci devient orange, celà signifie que vous
            voulez exclure ce phonème. Si vous cliquez une troisième fois ce
            phonème ne sera plus sélectionné.
          </li>
          <li>
            A chaque fois que vous selectionnez un phonème à inclure ou exclure,
            une section "POSITIONS" va apparaître au bas du formulaire. Il vous
            permet de définir dans quelle position le phonème doit être inclus
            ou exclus. Voici une explication des positions possibles
            <ul>
              <li>Partout: celà comprend tout le mot</li>
              <li>
                Médian: tout le mot en dehors des phonèmes de début et de fin
              </li>
              <li>Début: Premier phonème du mot</li>
              <li>Fin: Dernier phonème du mot</li>
            </ul>
          </li>
          <li>
            Si vous rencontrez un réel problème merci de nous contacter via{' '}
            <a rel='noopener noreferrer' target='_blank' href={TWITTER}>
              Twitter
            </a>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default memo(Help)
