import React from 'react'

import './Logo.scss'

type LogoProps = {}

function Logo (props: LogoProps): JSX.Element {
  return (
    <a className='Logo' href='/'>
      <img src='logo2.png' alt='logo' />
    </a>
  )
}

export default Logo
