export type SearchData = {
  isSaved: boolean
  structure: string
  inclusions: {
    init: string[]
    median: string[]
    end: string[]
  }
  exclusions: {
    init: string[]
    median: string[]
    end: string[]
  }
  filters: string[]
}
