export type Word = {
  _id: string
  text: string
  pron: string
  structure: string
  freq?: number
  aoa?: number
  type?: string
}
