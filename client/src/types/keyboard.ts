export enum LetterState {
  None = 0,
  Include,
  Exclude
}

export type Letter = {
  char: string
  state: LetterState
}
