export function arrayChunk<T> (inputArray: T[], chunkSize: number = 1): T[][] {
  const chunks: T[][] = []
  const inputArrayCopy: T[] = [...inputArray]
  while (inputArrayCopy.length) {
    chunks.push(inputArrayCopy.splice(0, chunkSize))
  }
  return chunks
}
