import { Positions } from '../shared/Position/Position'
import { LetterState } from '../types/keyboard'

function paramsToPostition (pos: string) {
  switch (pos) {
    case 'init':
      return Positions.START
    case 'median':
      return Positions.MEDIAN
    case 'end':
      return Positions.END
    default:
      return Positions.MEDIAN
  }
}

export function formatPositionsToKeys (inclusions: any, exclusions: any) {
  const res: any = {}

  Object.keys(inclusions).forEach((pos: string) => {
    const position = paramsToPostition(pos)
    inclusions[pos].forEach((char: string) => {
      const state = LetterState.Include
      res[char] = { state, position }
    })
  })

  Object.keys(exclusions).forEach((pos: string) => {
    const position = paramsToPostition(pos)
    exclusions[pos].forEach((char: string) => {
      const state = LetterState.Exclude
      res[char] = { state, position }
    })
  })
  return res
}
