import { PUBLIC_MEDIA_PATH_PATTERN } from './../constants'
export enum PublicMediaTypes {
  ILLUSTRATIONS = 'illustrations',
  ICONS = 'icons'
}

export enum PublicMediaExtensions {
  SVG = 'svg',
  PNG = 'png',
  JPG = 'jpg'
}

/**
 * Replace global public media pattern
 * @param name the filename
 * @param mediaType what kind of media ?
 * @param ext what extension
 */
export const getPublicMediaPath = (
  name: string,
  mediaType: PublicMediaTypes = PublicMediaTypes.ICONS,
  ext: PublicMediaExtensions = PublicMediaExtensions.SVG
) => {
  return PUBLIC_MEDIA_PATH_PATTERN.replace('{name}', name)
    .replace('{type}', mediaType)
    .replace('{ext}', ext)
}
