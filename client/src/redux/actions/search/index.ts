import {
  RangeFilters,
  SearchFormState,
  TextFilters
} from './../../reducers/search/form/form'
import { LetterState } from './../../../types/keyboard'
import { KeyboardState } from './../../../shared/Keyboard/Keyboard'
import {
  CLEAR_SEARCH_HISTORY_ERROR,
  CLEAR_SEARCH_HISTORY_STARTED,
  CLEAR_SEARCH_HISTORY_SUCCESS,
  FETCH_SEARCH_HISTORY_ERROR,
  FETCH_SEARCH_HISTORY_STARTED,
  FETCH_SEARCH_HISTORY_SUCCESS,
  NEW_KEY_POSITION,
  NEW_KEY_STATE,
  SEARCH_STARTED,
  SEARCH_SUCCESS,
  INIT_SEARCH_FORM,
  RESET_SEARCH_FORM,
  UPDATE_SEARCH_FORM,
  UPDATE_SEARCH_FILTERS,
  UPDATE_SEARCH_FILTERS_RANGES,
  UPDATE_SEARCH_FILTERS_TEXT,
  SEARCH_RESET
} from './../actions-types'
import { postSearchForm } from '../../../libs/word-api'
import {
  deleteSearchesHistory,
  fetchLatestSearches
} from '../../../libs/search-api'
import { Positions } from '../../../shared/Position/Position'
import { searchValuesSelector } from '../../selectors/search-selectors'

const _searchStarted = () => ({ type: SEARCH_STARTED })
const _searchSuccess = (data: any) => ({ type: SEARCH_SUCCESS, data })
const _fetchLatestSearchesStarted = () => ({
  type: FETCH_SEARCH_HISTORY_STARTED
})
const _fetchLatestSearchesSuccess = (contents: any) => ({
  type: FETCH_SEARCH_HISTORY_SUCCESS,
  contents
})
const _fetchLatestSearchesError = (err: any) => ({
  type: FETCH_SEARCH_HISTORY_ERROR,
  err
})

const _clearSearchesHistoryStarted = () => ({
  type: CLEAR_SEARCH_HISTORY_STARTED
})

const _clearSearchesHistorySuccess = () => ({
  type: CLEAR_SEARCH_HISTORY_SUCCESS
})

const _clearSearchesHistoryError = () => ({
  type: CLEAR_SEARCH_HISTORY_ERROR
})

export function submitSearchForm (doNotSaveInHistory = false) {
  return async (dispatch: any, getState: any) => {
    const { params = {}, filters = {} } = searchValuesSelector(getState())
    dispatch(_searchStarted())
    const results = await postSearchForm(params, filters, doNotSaveInHistory)
    dispatch(_searchSuccess(results.data.words))
  }
}

export function getLatestSearches (nb = 5) {
  return async (dispatch: any) => {
    dispatch(_fetchLatestSearchesStarted())
    try {
      const results = await fetchLatestSearches(nb)
      dispatch(_fetchLatestSearchesSuccess(results.data))
    } catch (e) {
      dispatch(_fetchLatestSearchesError(e))
    }
  }
}

export function clearSearchesHistory () {
  return async (dispatch: any) => {
    dispatch(_clearSearchesHistoryStarted())
    try {
      await deleteSearchesHistory()
      dispatch(_clearSearchesHistorySuccess())
    } catch (e) {
      dispatch(_clearSearchesHistoryError())
    }
  }
}

export function newKeyState (params: { char: string; state: LetterState }) {
  return {
    type: NEW_KEY_STATE,
    payload: params
  }
}

export function newKeyPosition (params: { char: string; position: Positions }) {
  return {
    type: NEW_KEY_POSITION,
    payload: params
  }
}

export function initSearchForm (keys: KeyboardState) {
  return {
    type: INIT_SEARCH_FORM,
    payload: keys
  }
}

export function resetSearchForm () {
  return (dispatch: any) => {
    dispatch({ type: RESET_SEARCH_FORM })
    dispatch({ type: SEARCH_RESET })
  }
}

export function updateSearchForm (data: Partial<SearchFormState>) {
  return {
    type: UPDATE_SEARCH_FORM,
    payload: data
  }
}

export function updateSearchFilters (
  name: string,
  filter: RangeFilters | TextFilters
) {
  return {
    type: UPDATE_SEARCH_FILTERS,
    payload: {
      name,
      filter
    }
  }
}

export function updateSearchRangesFilters (filter: RangeFilters) {
  return {
    type: UPDATE_SEARCH_FILTERS_RANGES,
    payload: {
      filter
    }
  }
}

export function updateSearchTextFilters (filter: TextFilters) {
  return {
    type: UPDATE_SEARCH_FILTERS_TEXT,
    payload: {
      filter
    }
  }
}
