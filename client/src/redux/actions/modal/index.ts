import { MODAL_OPEN, ModalActionType, MODAL_CLOSE } from './../actions-types'
import { GenericModalTypes } from './../../reducers/modal/index'

function _openModal (modalType: GenericModalTypes, data: any): ModalActionType {
  return {
    type: MODAL_OPEN,
    data: {
      modalType,
      data
    }
  }
}

function _closeModal (modalType: GenericModalTypes): ModalActionType {
  return {
    type: MODAL_CLOSE,
    modalType
  }
}

export const openModal = (modalType: GenericModalTypes, data = {}) => {
  return _openModal(modalType, data)
}

export const closeModal = (modalType: GenericModalTypes) => {
  return _closeModal(modalType)
}
