import { GenericModalTypes } from './../reducers/modal/index'
export const SEARCH_STARTED = 'SEARCH/SEARCH_SUBMIT'
export const SEARCH_SUCCESS = 'SEARCH/SEARCH_SUCCESS'
export const SEARCH_ERROR = 'SEARCH/SEARCH_ERROR'
export const SEARCH_RESET = 'SEARCH/SEARCH_RESET'

// WORDS
export const SELECTION_ADD_WORD = 'SELECTION_ADD_WORD'
export const SELECTION_REMOVE_WORD = 'SELECTION_REMOVE_WORD'
export const SELECTION_RESET = 'SELECTION_RESET'
export const WORD_FETCH_DETAILS = 'WORD_FETCH_DETAILS'
export const WORD_FETCH_DETAILS_STARTED = 'WORD_FETCH_DETAILS_STARTED'
export const WORD_FETCH_DETAILS_SUCCESS = 'WORD_FETCH_DETAILS_SUCCESS'
export const WORD_FETCH_DETAILS_ERROR = 'WORD_FETCH_DETAILS_ERROR'
interface SelectionAddWordType {
  type: typeof SELECTION_ADD_WORD
  data: {
    id: string
    text?: string
  }
}
interface SelectionRemoveWordType {
  type: typeof SELECTION_REMOVE_WORD
  data: {
    id: string
    text?: string
  }
}

interface SelectionResetType {
  type: typeof SELECTION_RESET
}

interface WordFetchDetailsType {
  type: typeof WORD_FETCH_DETAILS
  data: any
}

interface WordFetchDetailsStartedType {
  type: typeof WORD_FETCH_DETAILS_STARTED
}

interface WordFetchDetailsErrorType {
  type: typeof WORD_FETCH_DETAILS_ERROR
  error: string
}
export type WordActionType =
  | SelectionAddWordType
  | SelectionRemoveWordType
  | WordFetchDetailsType
  | WordFetchDetailsStartedType
  | WordFetchDetailsErrorType
  | SelectionResetType

// MODALS
export const MODAL_OPEN = 'MODAL_OPEN'
export const MODAL_CLOSE = 'MODAL_CLOSE'
interface ModalOpenType {
  type: typeof MODAL_OPEN
  data: {
    modalType: GenericModalTypes
    data: any
  }
}
interface ModalCloseType {
  type: typeof MODAL_CLOSE
  modalType: GenericModalTypes
}

export type ModalActionType = ModalOpenType | ModalCloseType

// AUTH
export type UserData = {
  id: string
  isPremium: boolean
  createdAt: Date
  googleId: string
  displayName: string
  email: string
  avatar: string
}
export const FETCH_USER_STARTED = 'FETCH_USER_STARTED'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR'
export type FetchUserStartedActionType = {
  type: typeof FETCH_USER_STARTED
}

export type FetchUserSuccessActionType = {
  type: typeof FETCH_USER_SUCCESS
  user: UserData
}

export type FetchUserErrorActionType = {
  type: typeof FETCH_USER_ERROR
  error: Error
}

export type AuthActionType =
  | FetchUserStartedActionType
  | FetchUserSuccessActionType
  | FetchUserErrorActionType

// SEARCH

export const FETCH_SEARCH_HISTORY_STARTED = 'FETCH_SEARCH_HISTORY_STARTED'
export const FETCH_SEARCH_HISTORY_SUCCESS = 'FETCH_SEARCH_HISTORY_SUCCESS'
export const FETCH_SEARCH_HISTORY_ERROR = 'FETCH_SEARCH_HISTORY_ERROR'
export const CLEAR_SEARCH_HISTORY_STARTED = 'CLEAR_SEARCH_HISTORY_STARTED'
export const CLEAR_SEARCH_HISTORY_SUCCESS = 'CLEAR_SEARCH_HISTORY_SUCCESS'
export const CLEAR_SEARCH_HISTORY_ERROR = 'CLEAR_SEARCH_HISTORY_ERROR'

export const NEW_KEY_STATE = 'NEW_KEY_STATE'
export const NEW_KEY_POSITION = 'NEW_KEY_POSITION'
export const INIT_SEARCH_FORM = 'INIT_SEARCH_FORM'
export const RESET_SEARCH_FORM = 'RESET_SEARCH_FORM'
export const SUBMIT_SEARCH_FORM = 'SUBMIT_SEARCH_FORM'
export const UPDATE_SEARCH_FORM = 'UPDATE_SEARCH_FORM'
export const UPDATE_SEARCH_FILTERS = 'UPDATE_SEARCH_FILTERS'
export const UPDATE_SEARCH_FILTERS_RANGES = 'UPDATE_SEARCH_FILTERS_RANGES'
export const UPDATE_SEARCH_FILTERS_TEXT = 'UPDATE_SEARCH_FILTERS_TEXT'
