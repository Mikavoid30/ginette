import axios from 'axios'
import {
  FETCH_USER_STARTED,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR
} from '../actions-types'

export const fetchUser = () => async (dispatch: any) => {
  dispatch({ type: FETCH_USER_STARTED })
  try {
    const user = await axios.get('/auth/current_user')
    dispatch({
      type: FETCH_USER_SUCCESS,
      user: user?.data || false
    })
  } catch (e) {
    dispatch({ type: FETCH_USER_ERROR })
  }
}
