import {
  WORD_FETCH_DETAILS,
  WORD_FETCH_DETAILS_STARTED,
  WordActionType,
  WORD_FETCH_DETAILS_ERROR
} from './../actions-types'
import { getWordDetails } from '../../../libs/word-api'

function _fetchWordDetails (data: any): WordActionType {
  return {
    type: WORD_FETCH_DETAILS,
    data
  }
}

function _fetchWordDetailsError (message: any): WordActionType {
  return {
    type: WORD_FETCH_DETAILS_ERROR,
    error: message
  }
}

function _fetchWordDetailsStarted (): WordActionType {
  return {
    type: WORD_FETCH_DETAILS_STARTED
  }
}

export function fetchWordDetails (id: string) {
  return async (dispatch: any) => {
    dispatch(_fetchWordDetailsStarted())
    try {
      const data = await getWordDetails(id)
      return dispatch(_fetchWordDetails(data))
    } catch (e) {
      return dispatch(_fetchWordDetailsError(e.message))
    }
  }
}
