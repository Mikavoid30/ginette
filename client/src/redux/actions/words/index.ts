import * as wordSelectionActions from './word-selection'
import * as wordActions from './word'

export const resetWordSelection = wordSelectionActions.resetWordSelection
export const addWordToSelection = wordSelectionActions.addWordToSelection
export const removeWordFromSelection =
  wordSelectionActions.removeWordFromSelection
export const fetchWordDetails = wordActions.fetchWordDetails
