import {
  SELECTION_ADD_WORD,
  SELECTION_REMOVE_WORD,
  SELECTION_RESET
} from './../actions-types'

export type wordSelectionData = {
  text: string
  pron: string
  id: string
}
function _addWordToSelection (data: wordSelectionData) {
  return {
    type: SELECTION_ADD_WORD,
    data
  }
}

function _removeWordFromSelection (data: wordSelectionData) {
  return {
    type: SELECTION_REMOVE_WORD,
    data
  }
}

function _resetWordSelection () {
  return {
    type: SELECTION_RESET
  }
}

export const addWordToSelection = (data: wordSelectionData) => {
  return _addWordToSelection(data)
}

export const removeWordFromSelection = (data: wordSelectionData) => {
  return _removeWordFromSelection(data)
}

export const resetWordSelection = () => {
  return _resetWordSelection()
}
