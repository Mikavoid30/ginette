import {
  ModalActionType,
  MODAL_CLOSE,
  MODAL_OPEN
} from './../../actions/actions-types'

export enum GenericModalTypes {
  DEFAULT = 'Generic',
  DIAPORAMA = 'Diaporama'
}

export type ModalState = {
  open: boolean
  modalType: GenericModalTypes
  data: any
}
export const initialState: ModalState = {
  open: false,
  modalType: GenericModalTypes.DEFAULT,
  data: {}
}

export default (state = initialState, action: ModalActionType) => {
  switch (action.type) {
    case MODAL_OPEN:
      return {
        ...state,
        open: true,
        modalType: action.data.modalType,
        data: action.data.data
      }
    case MODAL_CLOSE:
      return { ...state, open: false }
    default:
      return state
  }
}
