import {
  AuthActionType,
  FETCH_USER_SUCCESS,
  FETCH_USER_STARTED,
  FETCH_USER_ERROR
} from './../../actions/actions-types'

const initialState = {
  loading: true,
  authed: null,
  avatar: '#',
  error: null
}

export default (state = initialState, action: AuthActionType) => {
  switch (action.type) {
    case FETCH_USER_STARTED:
      return {
        ...state,
        loading: true
      }
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        authed: action.user,
        avatar: action.user.avatar,
        loading: false
      }
    case FETCH_USER_ERROR:
      return {
        ...state,
        authed: false,
        error: action.error,
        loading: false
      }
    default:
      return state
  }
}
