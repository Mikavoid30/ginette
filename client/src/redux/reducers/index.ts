import { combineReducers } from 'redux'
import search from './search'
import words from './words'
import modal from './modal'
import auth from './auth'

const app = combineReducers({
  search,
  words,
  modal,
  auth
})

export default app
