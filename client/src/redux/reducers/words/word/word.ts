import {
  WordActionType,
  WORD_FETCH_DETAILS,
  WORD_FETCH_DETAILS_ERROR,
  WORD_FETCH_DETAILS_STARTED
} from './../../../actions/actions-types'
type WordDetailsState = {
  loading: boolean
  error?: string
  images?: any[]
  word: {
    text?: string
    pron?: string
    freq?: number
    aoa?: number
    type?: string
    id?: string
  }
}

const initialState: WordDetailsState = {
  word: {},
  images: [],
  loading: false,
  error: ''
}

export default function wordFetchDetails (
  state: WordDetailsState = initialState,
  action: WordActionType
) {
  switch (action.type) {
    case WORD_FETCH_DETAILS:
      return { ...state, ...action.data, loading: false, error: '' }
    case WORD_FETCH_DETAILS_STARTED:
      return { ...state, loading: true, error: '' }
    case WORD_FETCH_DETAILS_ERROR:
      return { ...state, loading: false, error: action.error }
    default:
      return state
  }
}
