import {
  WordActionType,
  SELECTION_ADD_WORD,
  SELECTION_REMOVE_WORD,
  SELECTION_RESET
} from './../../../actions/actions-types'
type WordSelectionState = {
  text: string
  id: string
  pron: string
}[]

const initialState: WordSelectionState = []

export default function wordSelection (
  state: WordSelectionState = initialState,
  action: WordActionType
) {
  switch (action.type) {
    case SELECTION_ADD_WORD:
      if (state.some(x => x.id === action.data.id)) return state
      return [action.data, ...state]
    case SELECTION_REMOVE_WORD:
      return [...state].filter(x => x.id !== action.data.id)
    case SELECTION_RESET:
      return initialState
    default:
      return state
  }
}
