import { combineReducers } from 'redux'
import wordSelectionReducer from './word-selection/word-selection'
import wordDetailsReducer from './word/word'

const wordsReducer = combineReducers({
  selection: wordSelectionReducer,
  wordDetails: wordDetailsReducer
})

export default wordsReducer
