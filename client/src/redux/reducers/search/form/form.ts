import {
  INIT_SEARCH_FORM,
  RESET_SEARCH_FORM,
  UPDATE_SEARCH_FORM
} from './../../../actions/actions-types'
import { LetterState } from './../../../../types/keyboard'
import { KeyboardState } from './../../../../shared/Keyboard/Keyboard'
import {
  NEW_KEY_STATE,
  NEW_KEY_POSITION,
  UPDATE_SEARCH_FILTERS,
  UPDATE_SEARCH_FILTERS_RANGES,
  UPDATE_SEARCH_FILTERS_TEXT
} from '../../../actions/actions-types'
import { Positions } from '../../../../shared/Position/Position'

export type TextFilters = {
  [filterName: string]: string[]
}

export type RangeFilters = {
  [filterName: string]: number[]
}

export type Filters = {
  ranges?: RangeFilters
  text?: TextFilters
}
export type SearchFormState = {
  structure: string
  keys: KeyboardState
  filters: Filters
}
const initialState: SearchFormState = {
  structure: '',
  filters: {
    ranges: {},
    text: {}
  },
  keys: {}
}
const searchForm = (state = initialState, { payload, type }: any) => {
  switch (type) {
    case INIT_SEARCH_FORM:
      return {
        ...state,
        ...payload
      }
    case RESET_SEARCH_FORM:
      return {
        ...initialState
      }
    case UPDATE_SEARCH_FORM:
      return {
        ...state,
        ...payload
      }
    case UPDATE_SEARCH_FILTERS:
      return {
        ...state,
        filters: {
          ...state.filters,
          [payload.name]: {
            ...payload.filter
          }
        }
      }
    case UPDATE_SEARCH_FILTERS_RANGES:
      return {
        ...state,
        filters: {
          ...state.filters,
          ranges: {
            ...state.filters.ranges,
            ...payload.filter
          }
        }
      }
    case UPDATE_SEARCH_FILTERS_TEXT:
      return {
        ...state,
        filters: {
          ...state.filters,
          text: {
            ...state.filters.text,
            ...payload.filter
          }
        }
      }
    case NEW_KEY_STATE:
      const keys = { ...state.keys }
      if (keys[payload.char]) {
        if (payload.state === 0) {
          delete keys[payload.char]
        } else {
          keys[payload.char] = {
            ...keys[payload.char],
            state: payload.state
          }
        }
      } else {
        keys[payload.char] = {
          ...keys[payload.char],
          state: LetterState.Include
        }
      }
      return {
        ...state,
        keys
      }
    case NEW_KEY_POSITION:
      const ks = { ...state.keys }

      if (ks[payload.char]) {
        ks[payload.char] = {
          ...ks[payload.char],
          position: payload.position
        }
      } else {
        ks[payload.char] = {
          ...ks[payload.char],
          position: Positions.MEDIAN
        }
      }
      return {
        ...state,
        keys: ks
      }

    default:
      return state
  }
}
export default searchForm
