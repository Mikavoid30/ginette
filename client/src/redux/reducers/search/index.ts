import { combineReducers } from 'redux'
import searchHistoryReducer from './history/history'
import searchReducer from './search/search'
import searchForm from './form/form'

const wordsReducer = combineReducers({
  search: searchReducer,
  form: searchForm,
  searchHistory: searchHistoryReducer
})

export default wordsReducer
