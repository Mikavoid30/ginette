import {
  FETCH_SEARCH_HISTORY_STARTED,
  FETCH_SEARCH_HISTORY_SUCCESS,
  FETCH_SEARCH_HISTORY_ERROR,
  CLEAR_SEARCH_HISTORY_SUCCESS
} from './../../../actions/actions-types'
import { SearchData } from '../../../../types/search'

type SearchHistoryState = {
  loading: boolean
  contents: SearchData[]
}
const initialState: SearchHistoryState = {
  loading: false,
  contents: []
}
const searchHistory = (state = initialState, action: any) => {
  switch (action.type) {
    case FETCH_SEARCH_HISTORY_STARTED:
      return {
        ...state,
        loading: true
      }
    case FETCH_SEARCH_HISTORY_SUCCESS:
      return {
        ...state,
        loading: false,
        contents: action.contents
      }
    case FETCH_SEARCH_HISTORY_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error
      }
    case CLEAR_SEARCH_HISTORY_SUCCESS: {
      return {
        ...state,
        loading: false,
        contents: []
      }
    }
    default:
      return state
  }
}
export default searchHistory
