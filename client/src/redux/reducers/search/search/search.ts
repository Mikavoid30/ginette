import {
  SEARCH_ERROR,
  SEARCH_STARTED,
  SEARCH_SUCCESS,
  SEARCH_RESET
} from './../../../actions/actions-types'
import { Word } from '../../../../types/words'

type SearchState = {
  isDirty: boolean
  loading: boolean
  contents: Word[]
}
const initialState: SearchState = {
  loading: false,
  isDirty: false,
  contents: []
}
const search = (state = initialState, action: any) => {
  switch (action.type) {
    case SEARCH_RESET:
      return {
        ...initialState
      }
    case SEARCH_STARTED:
      return {
        ...state,
        loading: true,
        isDirty: true
      }
    case SEARCH_SUCCESS:
      return {
        ...state,
        loading: false,
        contents: action.data,
        isDirty: true
      }
    case SEARCH_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error,
        isDirty: true
      }
    default:
      return state
  }
}
export default search
