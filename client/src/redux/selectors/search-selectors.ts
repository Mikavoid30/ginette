import { KeyboardState } from '../../shared/Keyboard/Keyboard'
import { Positions } from '../../shared/Position/Position'
import { LetterState } from '../../types/keyboard'

function formatPositionsForSubmitting (keys: KeyboardState) {
  return Object.keys(keys).reduce(
    (acc: any, curr: any) => {
      const state: LetterState = keys[curr].state || 0
      const pos = keys[curr].position
      let type = 'inclusions'
      if (state === LetterState.Exclude) type = 'exclusions'

      let posKey: 'median' | 'init' | 'end' = 'median'
      if (pos === Positions.END) posKey = 'end'
      if (pos === Positions.START) posKey = 'init'
      acc[type][posKey].push(curr as any)
      return acc
    },
    {
      inclusions: {
        init: [],
        median: [],
        end: []
      },
      exclusions: {
        init: [],
        median: [],
        end: []
      }
    }
  )
}

export const searchValuesSelector = (state: any) => {
  const { keys, structure, filters } = state.search.form
  const params = {
    structure,
    ...formatPositionsForSubmitting(keys)
  }
  return { filters, params }
}
