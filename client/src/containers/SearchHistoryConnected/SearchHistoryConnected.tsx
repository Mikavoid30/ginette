import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  clearSearchesHistory,
  getLatestSearches,
  initSearchForm,
  submitSearchForm
} from '../../redux/actions/search'
import { useNavigate } from 'react-router-dom'
import SearchHistory from '../../shared/SearchHistory/SearchHistory'

type SearchHistoryConnectedProps = {}

function SearchHistoryConnected (
  props: SearchHistoryConnectedProps
): JSX.Element {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const history = useSelector(
    (state: any) => state.search.searchHistory.contents
  )

  const state = {
    history,
    navigate
  }

  const actions = {
    getLatestSearches: (nb: number) => dispatch(getLatestSearches(nb)),
    clearSearchesHistory: () => dispatch(clearSearchesHistory()),
    initSearchForm: (state: any) => dispatch(initSearchForm(state)),
    submitSearchForm: (doNotSaveInHistory: boolean) =>
      dispatch(submitSearchForm(doNotSaveInHistory))
  }
  const newProps = { ...props, ...actions, ...state }
  return <SearchHistory {...newProps} />
}

export default SearchHistoryConnected
