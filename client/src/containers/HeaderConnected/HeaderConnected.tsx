import React from 'react'
import Header from '../../shared/Header/Header'
import { useSelector } from 'react-redux'

type HeaderConnectedProps = { children?: JSX.Element | JSX.Element[] | null }

function HeaderConnected (props: HeaderConnectedProps): JSX.Element {
  const isLoggedIn = useSelector((state: any) => state.auth.authed)
  const isLoading = useSelector((state: any) => state.auth.loading)
  const error = useSelector((state: any) => state.auth.error)
  const avatar = useSelector((state: any) => state.auth.avatar)

  const newProps = { ...props, isLoggedIn, avatar, isLoading, error }
  return <Header {...newProps} />
}

export default HeaderConnected
