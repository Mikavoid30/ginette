import React from 'react'
import { useNavigate } from 'react-router-dom'

import './Login.scss'

type LoginProps = {
  isLoggedIn?: boolean
}

function Login ({ isLoggedIn }: LoginProps): JSX.Element {
  const navigate = useNavigate()
  return (
    <div className='Login'>
      <h1>Login</h1>
      {isLoggedIn ? (
        <button>logout</button>
      ) : (
        <button
          onClick={() => {
            navigate('auth/google')
          }}
        >
          login with google
        </button>
      )}
    </div>
  )
}

export default Login
