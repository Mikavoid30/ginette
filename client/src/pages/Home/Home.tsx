import React from 'react'
import Button, { ButtonSize } from '../../shared/Button/Button'
import Hero from '../../shared/Hero/Hero'
import PresentationScreen from '../../shared/PresentationScreen/PresentationScreen'

import './Home.scss'

type HomeProps = { isLoggedIn?: boolean }

function Home ({ isLoggedIn }: HomeProps): JSX.Element {
  return (
    <div className='Home'>
      <Hero />
      <section className='Home_screen'>
        <PresentationScreen />
      </section>
      <section className='Home_cta'>
        <Button size={ButtonSize.BIG} filled isLink href='/search'>
          Lancer une recherche
        </Button>
      </section>
      {isLoggedIn ? (
        <a href='/auth/logout'>Logout</a>
      ) : (
        <a href='/auth/google'>Login with google</a>
      )}
    </div>
  )
}

export default Home
