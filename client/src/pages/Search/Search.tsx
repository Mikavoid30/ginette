import React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import SearchForm from '../../shared/SearchForm/SearchForm'
import { openModal } from '../../redux/actions/modal'
import {
  addWordToSelection,
  removeWordFromSelection,
  resetWordSelection
} from '../../redux/actions/words'
import WordList from '../../shared/WordList/WordList'
import EmptyPlaceholder from '../../shared/EmptyPlaceholder/EmptyPlaceholder'
import LoadingSpinner from '../../shared/LoadingSpinner/LoadingSpinner'
import SearchToolbar from '../../shared/SearchToolbar/SearchToolbar'
import './Search.scss'
import { wordSelectionData } from '../../redux/actions/words/word-selection'
import { GenericModalTypes } from '../../redux/reducers/modal'
import SelectionList from '../../shared/SelectionList/SelectionList'
import axios from 'axios'
import { searchValuesSelector } from '../../redux/selectors/search-selectors'
import SearchHistoryConnected from '../../containers/SearchHistoryConnected/SearchHistoryConnected'
import Help from '../../shared/Help/Help'
import classnames from 'classnames'

type SearchProps = {
  isLoggedIn?: boolean
}

function onSaveList (ids: string[], groupId = null) {
  axios
    .post('/api/v1/list', { ids, group: '60a3a74e73ef96264d78ad87' })
    .then(res => {
      console.log('Saved', res.data)
    })
    .catch(e => console.log('cannot download pdf', e))
}

function downloadPdfFromList (ids: string[] = []) {
  axios
    .post('/api/v1/pdf/fromList', { ids })
    .then(res => {
      if (res?.data.token?.length > 0) {
        window.open(`/api/v1/pdf/download/${res.data.token}`, '_blank')
      }
    })
    .catch(e => console.log('cannot download pdf', e))
}

function _renderEmptyPlaceholder (
  firstEntry: boolean,
  isLoggedIn: boolean
): JSX.Element {
  return firstEntry ? (
    isLoggedIn ? (
      <div className='Search-grid'>
        <SearchHistoryConnected />
        <SearchHistoryConnected />
      </div>
    ) : (
      <EmptyPlaceholder>
        <h2>Phonemia</h2>
        <p>Vous pouvez démarrer une nouvelle recherche</p>
        <Help />
      </EmptyPlaceholder>
    )
  ) : (
    <EmptyPlaceholder illustrationName='notFound'>
      <h2>Pas de résultats :(</h2>
      <p>Votre recherche est peut-être trop pécise</p>
    </EmptyPlaceholder>
  )
}
function Search (props: SearchProps): JSX.Element {
  const dispatch = useDispatch()
  const firstEntry = useSelector((state: any) => !state.search.search.isDirty)
  const wordSelection = useSelector((state: any) => state.words.selection)
  const contents = useSelector((state: any) => state.search.search.contents)
  const loading = useSelector((state: any) => state.search.search.loading)
  const isLoggedIn = useSelector((state: any) => state.auth.authed)

  const handleWordSelected = (data: wordSelectionData) => {
    if (!data?.id) return
    dispatch(addWordToSelection(data))
  }

  const handleWordUnSelected = (data: wordSelectionData) => {
    if (!data?.id) return
    dispatch(removeWordFromSelection(data))
  }

  return (
    <div
      className={classnames(
        `Search ${firstEntry ? 'Search-first-entry' : 'Search-dirty'}`,
        { ['Search--is-selection-open']: wordSelection?.length > 0 }
      )}
    >
      <section className='Layout_aside'>
        <SearchForm />
      </section>
      <section className='Layout_main'>
        <SearchToolbar
          firstEntry={!!firstEntry}
          contentsLength={contents.length}
          wordSelection={wordSelection}
          onResetSelection={() => {
            dispatch(resetWordSelection())
          }}
          onDiaporamaClicked={() =>
            dispatch(openModal(GenericModalTypes.DIAPORAMA))
          }
        />
        <main>
          <div className='body-container'>
            {loading ? (
              <LoadingSpinner />
            ) : contents && contents.length ? (
              <WordList
                wordSelection={wordSelection}
                list={contents}
                onWordSelected={handleWordSelected}
                onWordUnSelected={handleWordUnSelected}
              />
            ) : (
              _renderEmptyPlaceholder(firstEntry, isLoggedIn)
            )}
          </div>
        </main>
      </section>
      <section className={classnames('Search-selection-column')}>
        {wordSelection.length ? (
          <>
            <SelectionList
              list={wordSelection}
              onResetSelection={() => {
                dispatch(resetWordSelection())
              }}
              onDownloadPdfFromList={downloadPdfFromList}
              onSaveList={onSaveList}
              onRemoveWordFromSelection={(data: wordSelectionData) => {
                handleWordUnSelected(data)
              }}
            />
          </>
        ) : null}
      </section>
    </div>
  )
}

export default Search
