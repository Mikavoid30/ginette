import React from 'react'
import Title from '../../shared/Title/Title'

import './Dashboard.scss'

type DashboardProps = {
  isLoggedIn: boolean
}

function Dashboard (props: DashboardProps): JSX.Element {
  return (
    <div className='Dashboard'>
      <section className='section'>
        <div>
          <Title level={1}>Tableau de bord</Title>
          <div className='grid'>DASHBOARD</div>
        </div>
      </section>
    </div>
  )
}

export default Dashboard
