import React from 'react'

import './NoMatch.scss'

type NoMatchProps = {}

function NoMatch (props: NoMatchProps): JSX.Element {
  return (
    <div className='NoMatch'>
      <h1>404 NOT FOUND</h1>
    </div>
  )
}

export default NoMatch
