export const CONSONANTS = [
  'b',
  'd',
  'f',
  'ɡ',
  'k',
  'l',
  'm',
  'n',
  'ɲ',
  'p',
  'ʁ',
  's',
  'ʃ',
  't',
  'v',
  'z',
  'ʒ',
  'j',
  'w',
  'ɥ'
]
export const VOWELS = [
  'a',
  'ɑ',
  'e',
  'ɛ',
  'ɛː',
  'ə',
  'i',
  'œ',
  'ø',
  'o',
  'ɔ',
  'u',
  'y',
  'ɑ̃',
  'ɛ̃',
  'œ̃',
  'ɔ̃'
]

export const PARAMS_INCLUSIONS_ARRAY_NAME = 'inclusions'
export const PARAMS_EXCLUSIONS_ARRAY_NAME = 'exclusions'
export const PARAMS_POSITION_START = 'init'
export const PARAMS_POSITION_END = 'end'
export const PARAMS_POSITION_MEDIAN = 'median'
export const PARAMS_POSITION_ANY = 'any'

export const INCLUSION_COLOR = '#00d6e3'
export const EXCLUSION_COLOR = '#ffcf6d'

export const PUBLIC_MEDIA_TYPE_PATTERN = '{type}'
export const PUBLIC_MEDIA_NAME_PATTERN = '{name}'
export const PUBLIC_MEDIA_EXT_PATTERN = '{ext}'
export const PUBLIC_MEDIA_PATH_PATTERN = `/${PUBLIC_MEDIA_TYPE_PATTERN}/${PUBLIC_MEDIA_NAME_PATTERN}.${PUBLIC_MEDIA_EXT_PATTERN}`

export const KEY_ARROW_RIGHT = 'ArrowRight'
export const KEY_ARROW_LEFT = 'ArrowLeft'
export const KEY_ESCAPE = 'Escape'

export const VALID_STUCTURE_CHARACTERS = ['C', 'V', '-']

export const WORD_TYPES: { [k: string]: string } = {
  NOM: 'NOM',
  VER: 'VER',
  ADJ: 'ADJ',
  ONO: 'ONO',
  PRO: 'PRO',
  ART: 'ART',
  ADV: 'ADV',
  PRE: 'PRE'
}

export enum FILTER_NAMES {
  AOA = 'aoaMean',
  TYPE = 'type',
  FREQ = 'freq'
}

export const FILTER_FIELDS = {
  AOA: {
    name: FILTER_NAMES.AOA,
    label: 'AoA',
    title: "Age d'aquisition",
    initialValue: [0, 0]
  },
  TYPE: {
    name: FILTER_NAMES.TYPE,
    label: 'Type',
    title: 'Type de mot',
    initialValue: Object.keys(WORD_TYPES).map(x => ({
      text: WORD_TYPES[x]
    }))
  },
  FREQ: {
    name: FILTER_NAMES.FREQ,
    label: 'Freq',
    title: "Fréquence d'utilisation",
    initialValue: [0, 0]
  }
}

export const TWITTER = 'https://www.twitter.com/BoulatMickael'
