import React, { useEffect } from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import './App.scss'
import Login from './pages/Login/Login'
import Search from './pages/Search/Search'
import Home from './pages/Home/Home'
import NoMatch from './pages/_NoMatch/NoMatch'
import * as actions from './redux/actions/auth'
import Layout from './shared/Layout/Layout'
import Modal from './shared/Modals/Modal'
import { connect } from 'react-redux'
import Dashboard from './pages/Dashboard/Dashboard'
// import Home from './pages/Home/Home'

type AppProps = {
  fetchUser: () => any
  isLoggedIn: boolean
}

function App ({ fetchUser, isLoggedIn }: AppProps) {
  useEffect(() => {
    fetchUser()
  }, [])
  return (
    <>
      <Modal />
      <Router>
        <Layout>
          <Routes>
            <Route path='/' element={<Home isLoggedIn={isLoggedIn} />} />
            <Route
              path='/search/*'
              element={<Search isLoggedIn={isLoggedIn} />}
            />
            <Route path='/login' element={<Login isLoggedIn={isLoggedIn} />} />
            <Route
              path='/dashboard'
              element={<Dashboard isLoggedIn={isLoggedIn} />}
            />
            <Route path='*' element={<NoMatch />} />
          </Routes>
        </Layout>
      </Router>
    </>
  )
}

function mapStateToProps (state: any) {
  return { isLoggedIn: !!state.auth.authed }
}
export default connect(mapStateToProps, actions)(App)
