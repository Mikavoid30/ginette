import { VALID_STRUCTURE_REGEX } from '../constants'

export function isValidStructure (structure = '') {
  if (!structure) return false
  return VALID_STRUCTURE_REGEX.test(structure.toUpperCase())
}
