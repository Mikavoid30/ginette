const { isValidStructure } = require('../words')

describe('Words Helper', () => {
  describe('isValidStructure', () => {
    it('given wrong structure it should give back false', () => {
      let structure = 'CVCO'
      expect(isValidStructure(structure)).toEqual(false)
      structure = ''
      expect(isValidStructure(structure)).toEqual(false)
      structure = 'CVC-O'
      expect(isValidStructure(structure)).toEqual(false)
      structure = 'CV-- CV'
      expect(isValidStructure(structure)).toEqual(false)
    })

    it('given good structure it should give back true', () => {
      let structure = 'CVC'
      expect(isValidStructure(structure)).toEqual(true)
      structure = 'CV-CV'
      expect(isValidStructure(structure)).toEqual(true)
      structure = 'CVC-CV-C-VCV-CCVCCV-VC'
      expect(isValidStructure(structure)).toEqual(true)
      structure = 'cv-cvc'
      expect(isValidStructure(structure)).toEqual(true)
    })
  })
})
