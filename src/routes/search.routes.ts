import { Router } from 'express'
import searchController from '../controllers/search.controller'

export default (router: Router) => {
  router.get('/', searchController.getAll)
  router.get('/latest', searchController.getLatest)
  router.get('/latest/:nb', searchController.getLatest)
  router.delete('/', searchController.deleteHistory)
  return router
}
