import express from 'express'
import wordRoutes from './word.routes'
import pdfRoutes from './pdf.routes'
import groupRoutes from './group.routes'
import listRoutes from './list.routes'
import searchRoutes from './search.routes'
import requireAuth from '../middleware/requireAuth'

// Routers
const groupRouter = groupRoutes(express.Router())
const pdfRouter = pdfRoutes(express.Router())
const wordRouter = wordRoutes(express.Router())
const listRouter = listRoutes(express.Router())
const searchRouter = searchRoutes(express.Router())

// Base router
const baseRouter = express.Router()
// -- Private
baseRouter.use('/group', requireAuth, groupRouter)
baseRouter.use('/list', requireAuth, listRouter)
baseRouter.use('/search', requireAuth, searchRouter)

// -- Public
baseRouter.use('/pdf', pdfRouter)
baseRouter.use('/words', wordRouter)

export default baseRouter
