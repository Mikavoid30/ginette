import { Request, Response, Router } from 'express'
import passport from 'passport'

export default (router: Router) => {
  router.get(
    '/google',
    passport.authenticate('google', {
      scope: ['profile', 'email']
    })
  )

  router.get(
    '/google/callback',
    passport.authenticate('google'),
    (req, res) => {
      res.redirect('/search')
    }
  )

  router.get('/current_user', (req: Request, res: Response) => {
    res.send(req.user)
  })

  router.get('/logout', (req: Request, res: Response) => {
    req.logout()
    res.redirect('/')
  })
  return router
}
