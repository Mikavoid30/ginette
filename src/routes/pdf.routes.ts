import { Router } from 'express'
import pdfController from '../controllers/pdf.controller'

export default (router: Router) => {
  router.post('/fromList', pdfController.createPdfFromList)
  router.get('/download/:token', pdfController.download)
  return router
}
