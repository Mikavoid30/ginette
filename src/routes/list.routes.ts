import { Router } from 'express'
import listController from '../controllers/list.controller'

export default (router: Router) => {
  router.get('/', listController.getAll)
  router.get('/:id', listController.getOne)
  router.post('/', listController.createList)
  return router
}
