import { Router } from 'express'

export default (router: Router) => {
  const wordsController = require('../controllers/word.controller')

  router.get('/', wordsController.index)
  router.post('/search', wordsController.search)
  router.get('/find', wordsController.find)
  return router
}
