import { Router } from 'express'
import groupController from '../controllers/group.controller'

export default (router: Router) => {
  router.get('/', groupController.getAll)
  router.get('/:id', groupController.getOne)
  router.post('/', groupController.createGroup)
  router.put('/:id', groupController.updateGroup)
  router.delete('/:id', groupController.deleteGroup)
  return router
}
