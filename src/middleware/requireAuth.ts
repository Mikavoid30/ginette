import { NextFunction, Request, Response } from 'express'

export default function requireAuth (
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (req.user) {
    next()
  } else {
    res.status(403).send()
  }
}
