import mongoose, { Schema } from 'mongoose'

const userSchema = new Schema({
  googleId: {
    type: String
  },
  displayName: {
    type: String
  },
  email: {
    type: String
  },
  avatar: {
    type: String
  },
  isPremium: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: new Date()
  }
})

mongoose.model('User', userSchema)
