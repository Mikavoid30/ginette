import mongoose, { Schema } from 'mongoose'
import md5 from 'md5'

type Position = {
  init: string[]
  median: string[]
  end: string[]
}

export type SearchModelAttributes = {
  user: any
  isSaved?: boolean
  displayName?: string
  createdAt?: Date
  structure?: string
  inclusions?: Position
  filters?: string
  nbResults?: number
  hash: string
}

const defaultPositions = {
  init: [],
  median: [],
  end: []
}

const defaultFilters = {
  ranges: [],
  text: []
}
const searchSchema = new Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    isSaved: {
      type: Boolean,
      default: false
    },
    displayName: {
      type: String,
      default: null
    },
    structure: {
      type: String,
      default: ''
    },
    exclusions: {
      type: {},
      default: defaultPositions
    },
    inclusions: {
      type: {},
      default: defaultPositions
    },
    filters: {
      type: {},
      default: defaultFilters
    },
    nbResults: {
      type: Number,
      default: 0
    },
    hash: {
      type: String,
      default: null
    }
  },
  { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } }
)

export function generateSearchHash (search: any) {
  return md5(
    JSON.stringify({
      structure: search.structure,
      exclusions: search.exclusions,
      inclusions: search.inclusions,
      filters: search.filters
    })
  )
}
mongoose.model('Search', searchSchema)
