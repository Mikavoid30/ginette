import mongoose, { Schema } from 'mongoose'

const wordSchema = new Schema({
  text: {
    type: String
  },
  pron: {
    type: String
  },
  syllabs: {
    type: Number
  },
  structure: {
    type: String
  },
  freqLem: {
    type: Number
  },
  freq: {
    type: Number
  },
  type: {
    type: String
  },
  aoaMean: {
    type: Number
  },
  aoaSd: {
    type: Number
  },
  freqSubMean: {
    type: Number
  },
  freqSubSd: {
    type: Number
  }
})

export default mongoose.model('Word', wordSchema)
