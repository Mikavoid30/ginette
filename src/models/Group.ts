import mongoose, { Schema } from 'mongoose'

const groupSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  displayName: {
    type: String
  },
  description: {
    type: String
  },
  lists: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Comment'
    }
  ]
})

mongoose.model('Group', groupSchema)
