import mongoose, { Schema } from 'mongoose'

const listSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  group: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Group',
    default: null
  },
  displayName: {
    type: String
  },
  description: {
    type: String
  },
  words: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Word'
    }
  ]
})

mongoose.model('List', listSchema)
