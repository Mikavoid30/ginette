import { Model } from 'mongoose'

const db = {
  Word: Model,
  User: Model,
  Group: Model,
  List: Model,
  Search: Model
}

db.Word = require('./Word')
db.User = require('./User')
db.Group = require('./Group')
db.List = require('./List')
db.Search = require('./Search')

export default db
