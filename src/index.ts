import express, { NextFunction, Request, Response } from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import axios from 'axios'
import { connectDB } from './database'
import initPassport from './services/passport'
import passport from 'passport'
import cookieSession from 'cookie-session'
import { config } from 'dotenv'
import BaseRouter from './routes'
import authRoutes from './routes/auth.routes'

const app = express()
config()

// require('./seed')
// COOKIES
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [process.env?.COOKIE_SECRET || '']
  })
)

app.use(passport.initialize())
app.use(passport.session())

// AXIOS CONF
axios.defaults.baseURL = process.env.API_BASE_URL
axios.defaults.headers.post['Content-Type'] = 'application/json'

// DB CONNECTION
connectDB().then(() => {
  initPassport()
})

// --- MIDDLEWARE TREAT REQUEST AS JSON
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// ---  LOGGER ONLY OUT OF TEST
if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('combined'))
}

// --- CORS
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Credentials', 'true')
  res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
  res.header(
    'Access-Control-Allow-Headers',
    'Authorization,Content-Type,Accept,Origin,Pragma,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,Keep-Alive,X-Requested-With,If-Modified-Since'
  )
  next()
})

// Routes
app.use('/auth', authRoutes(express.Router()))
app.use('/api/v1', BaseRouter)
// require('./routes')(app)

if (process.env.NODE_ENV === 'production') {
  // Express will serve up prodution assets
  app.use(express.static('client/build'))

  // Express will serve up index.html if doesnt recognize the route
  const path = require('path')
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
  })
}

// --- ERROR HANDLING
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  res.status(400).send({ error: err.message })
  next()
})

// --- LISTEN AND SERVE
const port = process.env.PORT || 8082
app.listen(port, function () {
  if (process.env.NODE_ENV !== 'test') {
    console.log('Server is listening on port ' + port)
  }
})

module.exports = app
