import PdfPrinter from 'pdfmake'
import FileManager from './fileManager'

type Theme = {
  colors: {
    borders?: string
    grey?: string
    dark?: string
    primary?: string
    secondary?: string
    word?: string
    structure?: string
    pron?: string
  }
  sizes: {
    strates?: number
  }
}

type WordData = {
  text: string
  pron: string
  structure: string
}

export default class PdfService {
  _printer: any = null
  _theme: Theme = { colors: {}, sizes: {} }

  constructor () {
    this._theme = { colors: {}, sizes: {} }
    this._initPrinter()
  }

  async generatePdf (
    data: any,
    token = '',
    theme: Theme = { colors: {}, sizes: {} }
  ) {
    const fileManager = new FileManager()
    this._theme = this._buildTheme(theme)
    const dd = this._buildPdfData(data)
    const pdfDoc = await this._printer.createPdfKitDocument(dd, {})

    // Will save a temp file and delete it after 10s
    await fileManager.saveAutoDeleteFile(token + '.pdf', pdfDoc)
    return token
  }

  /**
   * Build an entire cell containing expression, synonyms etc...
   * @param data
   */
  _buildExpressionRow (data: WordData) {
    // console.log({ data })
    // let img = {}
    // if (data.images?.length > 0) {
    //   console.log('DATA', data)
    //   img = {
    //     image: `${__dirname}/../../data/_TMP_/images/img-${data._id}.jpg`,
    //     fit: [100, 100]
    //   }
    // }
    return [
      {
        text: data.text,
        style: {
          color: this._theme.colors.word,
          fontSize: this._theme.sizes.strates,
          font: 'NotoSans'
        }
      },
      {
        text: data.pron,
        style: {
          color: this._theme.colors.pron,
          fontSize: this._theme.sizes.strates,
          font: 'NotoSans'
        }
      },
      {
        text: data.structure,
        style: {
          color: this._theme.colors.structure,
          fontSize: this._theme.sizes.strates,
          font: 'NotoSans'
        }
      }
    ]
  }

  _buildTextCell (text: string, style = {}) {
    return { ...style, style: 'fr', text }
  }

  _buildPdfData (data: any) {
    return {
      footer: (currentPage: number, pageCount: any) => {
        return {
          layout: 'noBorders',
          table: {
            widths: ['100%', '100%', '100%'],
            headerRows: 0,
            body: [
              [
                {
                  text: 'Powered by phonemia.com (ᵔᴥᵔ)',
                  color: this._theme.colors.grey || 'black',
                  alignment: 'left',
                  margin: [40, 0, 0, 0],
                  fontSize: 10,
                  font: 'NotoSans'
                },
                {
                  text: currentPage.toString() + ' / ' + pageCount,
                  alignment: 'right',
                  margin: [0, 0, 40, 0],
                  color: this._theme.colors.primary || '#ccc'
                }
              ]
            ]
          }
        }
      },
      content: [
        {
          layout: 'noBorders',
          style: 'header',
          table: {
            widths: ['80%', '121px'],
            headerRows: 0,
            body: [[[{ text: 'Liste Phonemia' || '', style: 'subheader' }]]]
          }
        },
        {
          style: 'mainTable',
          layout: {
            hLineColor: this._theme.colors.borders || 'gray',
            vLineColor: this._theme.colors.borders || 'gray',
            fillColor: (rowIndex: number) => {
              return rowIndex % 2 === 0 ? '#fafafa' : null
            }
          },
          table: {
            headerRows: 1,
            widths: Array(3).fill('33%'),
            body: [
              [
                {
                  text: 'MOT',
                  style: 'tableHeader'
                },
                {
                  text: 'PRONONCIATION',
                  style: 'tableHeader'
                },
                {
                  text: 'STRUCTURE SYNTAXIQUE',
                  style: 'tableHeader'
                }
              ],
              ...data.map((entry: any) => {
                return this._buildExpressionRow(entry)
              })
            ]
          }
        }
      ],
      styles: {
        header: {
          margin: [0, 0, 20, 0]
        },
        subheader: {
          fontSize: 16,
          color: this._theme.colors.primary,
          bold: true,
          margin: [5, 10, 10, 0]
        },
        languages: {
          bold: false,
          fontSize: 12,
          color: this._theme.colors.secondary || '#f3345d',
          margin: [5, 5, 10, 10]
        },
        mainTable: {
          margin: [0, 15, 0, 0],
          color: this._theme.colors.dark || '#333333',
          fontSize: 11
        },
        tableHeader: {
          bold: true,
          fontSize: 11,
          fillColor: this._theme.colors.primary || '#207e8f',
          color: 'white',
          margin: [5, 5, 5, 5]
        },
        custom: {
          fillColor: 'black'
        },
        fr: { font: 'NotoSans' } // French
      }
    }
  }

  _buildTheme (theme: Theme): Theme {
    return {
      colors: {
        borders: '#cccccc',
        primary: '#216479',
        secondary: '#F3345D',
        structure: '#F3345D',
        word: '#333333',
        pron: '#279097',
        grey: '#989999',
        dark: '#464646',
        ...theme.colors
      },
      sizes: {
        strates: 16,
        ...theme.sizes
      }
    }
  }

  _initPrinter () {
    this._printer = new PdfPrinter({
      Roboto: {
        normal: __dirname + '/fonts/Roboto-Regular.ttf',
        bold: __dirname + '/fonts/Roboto-Bold.ttf',
        italics: __dirname + '/fonts/Roboto-Italic.ttf',
        bolditalics: __dirname + '/fonts/Roboto-BoldItalic.ttf'
      },
      NotoSans: {
        normal: __dirname + '/fonts/NotoSans-Regular.ttf',
        bold: __dirname + '/fonts/NotoSans-Bold.ttf',
        bolditalics: __dirname + '/fonts/NotoSans-BoldItalic.ttf',
        italics: __dirname + '/fonts/NotoSans-Italic.ttf'
      }
    })
  }
}
