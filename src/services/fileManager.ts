import request from 'request'
import fs from 'fs'
import path from 'path'
import { ensureDirectoryExistence } from '../helpers/filesystem'

const TMP_DIR = path.join('data', '_TMP_')

type Entries = {
  text: string
  pron: string
}

type FileData = {
  meta: {
    title: string
    date: string
    author: string
  }
  entries: Entries[]
}

export default class FileManagerService {
  /**
   * Will create a file and then remove it after deleteTimeout secondes
   * @param filename
   * @param readable
   * @param deleteTimeout
   */
  async saveAutoDeleteFile (
    filename: string,
    readable: any = null,
    deleteTimeout = 30
  ) {
    if (!filename) return Promise.resolve()
    try {
      await ensureDirectoryExistence(TMP_DIR)
      const filePath = path.join(TMP_DIR, filename)
      readable = readable || fs.createReadStream(filePath)
      readable.pipe(fs.createWriteStream(filePath))
      readable.end()
      setTimeout(() => {
        try {
          fs.unlinkSync(filePath)
        } catch (e) {
          console.log('File was already deleted')
        }
      }, deleteTimeout * 1000)
      return Promise.resolve(filePath)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async downloadImageAndAutoRemove (
    url: string,
    path: string,
    deleteTimeout = 300
  ) {
    return new Promise(resolve => {
      request.head(url, (err, res, body) => {
        request(url)
          .pipe(fs.createWriteStream(path))
          .on('close', resolve)
      })
    }).then(() => {
      setTimeout(() => {
        fs.unlinkSync(path)
      }, deleteTimeout * 1000)
    })
  }

  removeFile (file: string) {
    if (!file) return
    return fs.unlinkSync(file)
  }
}
