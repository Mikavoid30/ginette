import passport from 'passport'
import mongoose from 'mongoose'
import { Strategy as GoogleStrategy } from 'passport-google-oauth20'

const User = mongoose.model('User')

passport.serializeUser((user: any, done) => {
  done(null, user.id)
})

passport.deserializeUser((userId: string, done) => {
  User.findById(userId)
    .then(user => {
      done(null, user)
    })
    .catch(e => done(e?.message))
})

// Init passport
export default () =>
  passport.use(
    new GoogleStrategy(
      {
        clientID: process.env?.GOOGLE_CLIENT_ID || '',
        clientSecret: process.env?.GOOGLE_SECRET || '',
        callbackURL: '/auth/google/callback'
      },
      (accessToken, refreshToken, profile, done) => {
        const { id, displayName, emails = [], photos = [] } = profile
        User.findOne({ googleId: id })
          .then(existingUser => {
            if (existingUser) {
              // we already have the user
              done(null, existingUser)
            } else {
              // we need to create
              new User({
                googleId: id,
                displayName,
                email: emails[0]?.value,
                avatar: photos[0]?.value
              })
                .save()
                .then(user => {
                  done(null, user)
                })
                .catch(e => {
                  done(e?.message)
                })
            }
          })
          .catch(e => {
            done(e?.message)
          })
      }
    )
  )
