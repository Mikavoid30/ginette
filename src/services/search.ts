import { generateSearchHash } from '../models/Search'
import { SearchData } from './../../client/src/types/search.d'
import mongoose from 'mongoose'

const Search = mongoose.model('Search')

export const recordSearchInHistory = async (
  data: SearchData,
  user: any,
  nbResults: number
) => {
  const hash = generateSearchHash(data)

  const searchData = {
    ...data,
    user: user.id,
    nbResults,
    hash
  }

  try {
    // control hash
    const lastSearch: any = await Search.findOne({
      user: user?.id
    }).sort({
      createdAt: 'desc'
    })

    if (!lastSearch || hash !== lastSearch.hash) {
      await new Search(searchData).save()
    }
  } catch (e) {
    console.log('ERR', e)
  }
}
