import { Request, Response } from 'express'

import mongoose from 'mongoose'

const Group = mongoose.model('Group')

export default {
  getAll: async (req: Request, res: Response) => {
    const user: any = req.user
    const groups = await Group.find({ user: user.id })
    return res.json(groups)
  },
  getOne: (req: Request, res: Response) => {
    const { id } = req.params
    return res.json({ getOne: id })
  },
  createGroup: async (req: Request, res: Response) => {
    const user: any = req.user
    const { displayName = '', description = '' } = req.body

    const group = await new Group({
      displayName,
      description,
      user: user.id
    }).save()
    return res.json({ id: group._id })
  },
  updateGroup: (req: Request, res: Response) => {
    const body = req.body
    return res.json({ updateGroup: body })
  },
  deleteGroup: (req: Request, res: Response) => {
    const { id } = req.params
    return res.json({ delete: id })
  }
}
