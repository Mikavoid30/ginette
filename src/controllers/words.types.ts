export type Positions = {
  median: string[]
  init: string[]
  end: string[]
}
