const { CONSONANTS, VOWELS } = require('../../constants')
const {
  globalRegexStringFromStructure,
  addDelimitersToRegexString,
  addExclusionsToRegexString,
  generateFindRegex,
  getFiltersRegexArray
} = require('../word.controller')

describe.skip('Word Controller', () => {
  const c = CONSONANTS.join('')
  const v = VOWELS.join('')
  describe('generateFindRegex', () => {
    const cs = 'bdfɡklmnɲpʁsʃtvzʒjwɥ'
    const vs = `aɑeɛɛːəiœøoɔuyɑ̃ɛ̃œ̃ɔ̃`
    test('should give back a regex with inclusions', () => {
      const params = {
        structure: 'CV-CV',
        exclusions: {
          any: [],
          median: [],
          init: ['d'],
          end: ['e']
        },
        inclusions: {
          any: [],
          median: [],
          init: ['b'],
          end: ['a']
        }
      }
      const expected = `[b][${v}].[${c}][a]`

      expect(generateFindRegex(params)).toEqual(expected)
    })
    test('should give back a regex with exclusions', () => {
      const params = {
        structure: 'CV-CV',
        exclusions: {
          any: ['i'],
          median: ['k'],
          init: ['d', 'b'],
          end: ['e', 'a']
        },
        inclusions: {
          any: [],
          median: [],
          init: [],
          end: []
        }
      }
      const expected = `[fɡklmnɲpʁsʃtvzʒjwɥ][aɑeɛɛːəœøoɔuyɑ̃ɛ̃œ̃ɔ̃].[bdfɡlmnɲpʁsʃtvzʒjwɥ][ɑɛɛːəœøoɔuyɑ̃ɛ̃œ̃ɔ̃]`

      expect(generateFindRegex(params)).toEqual(expected)
    })

    test('should give back a regex with exclusions', () => {
      const params = {
        structure: 'CV-CV',
        exclusions: {
          any: ['i'],
          median: ['k'],
          init: ['d', 'b'],
          end: ['e', 'a']
        },
        inclusions: {
          any: [],
          median: [],
          init: [],
          end: []
        }
      }
      const expected = `[fɡklmnɲpʁsʃtvzʒjwɥ][aɑeɛɛːəœøoɔuyɑ̃ɛ̃œ̃ɔ̃].[bdfɡlmnɲpʁsʃtvzʒjwɥ][ɑɛɛːəœøoɔuyɑ̃ɛ̃œ̃ɔ̃]`

      expect(generateFindRegex(params)).toEqual(expected)
    })
  })
})
