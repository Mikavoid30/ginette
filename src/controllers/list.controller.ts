import { Request, Response } from 'express'

import mongoose from 'mongoose'

const List = mongoose.model('List')

export default {
  getAll: async (req: Request, res: Response) => {
    const user: any = req.user
    const lists = await List.find({ user: user.id }).populate('words', 'text')
    return res.json(lists)
  },
  getOne: (req: Request, res: Response) => {
    const { id } = req.params
    return res.json({ getOne: id })
  },
  createList: async (req: Request, res: Response) => {
    const user: any = req.user
    const {
      displayName = '',
      description = '',
      group = null,
      ids: words = []
    } = req.body

    const list = await new List({
      displayName,
      description,
      user: user.id,
      group,
      words: words
    }).save()
    return res.json({ id: list._id })
  }
}
