import { recordSearchInHistory } from './../services/search'
// import { generateSearchHash, SearchModelAttributes } from './../models/Search'
// import { buildWordData } from '../../feeder/helpers'
import mongoose from 'mongoose'
import Word from '../models/Word'
import {
  ENCODED_CONSONANTS,
  ENCODED_VOWELS,
  DEFAULT_PAGINATION_TAKE,
  ENCODE_MAPPING
} from '../constants'
import { getImagesForAWord } from '../libs/images-api'
import { Positions } from './words.types'
import { NextFunction, Request, Response } from 'express'
import { ImageFormattedResponse } from '../libs/image-api.types'

const Search = mongoose.model('Search')

function filterToRegex (filterStrings: string[] = []) {
  const strings = filterStrings
    .map(s => `(${s.replace(/\*/g, '(.*)?')})`)
    .join('|')

  return `^${strings}$`
}

function getRangeFiltersArray (filters: { [s: string]: number[] }) {
  const filterKeys = Object.keys(filters)
  if (!filterKeys || !filterKeys.length) return []

  let res = []

  for (let i = 0, lgt = filterKeys.length; i < lgt; i++) {
    const [min = 0, max = 0] = filters[filterKeys[i]]
    if (min === 0 && max === 0) break
    let minMax: { $gte: number; $lte?: number } = { $gte: 0 }
    if (min > 0) minMax.$gte = min
    if (max > 0) minMax.$lte = max
    res.push({
      [filterKeys[i]]: {
        ...minMax
      }
    })
  }

  return res
}

function getFiltersRegexArray (filters: { [s: string]: string[] } = {}) {
  const filterKeys = Object.keys(filters)
  if (!filterKeys || !filterKeys.length) return []

  let res = []

  for (let i = 0, lgt = filterKeys.length; i < lgt; i++) {
    res.push({
      [filterKeys[i]]: {
        $regex: filterToRegex(filters[filterKeys[i]]),
        $options: 'i'
      }
    })
  }

  return res
}

function createDelimiter (
  inc: string[] = [],
  exc: string[] = [],
  alpha: string
) {
  console.log({ inc, exc }, !!exc.length)
  let arr = inc
  if (inc.length) {
    if (exc.length) {
      arr = inc.filter(x => !exc.includes(x))
    }
  } else {
    if (exc.length) {
      arr = alpha.split('').filter(x => !exc.includes(x))
    } else {
      return ''
    }
  }

  return `[${arr.join('')}]{1}`
}

function encodePrononciation (decodedPron = '') {
  let encoded = decodedPron

  if (!encoded) return ''

  for (let i = 0; i < ENCODE_MAPPING.length; i++) {
    encoded = encoded.replace(
      new RegExp(ENCODE_MAPPING[i][0], 'g'),
      ENCODE_MAPPING[i][1]
    )
  }

  return encoded
}

function encodePositions ({ init = [], median = [], end = [] }: Positions) {
  return {
    init: encodePrononciation(init.join(','))
      .split(',')
      .filter(x => x.length),
    median: encodePrononciation(median.join(','))
      .split(',')
      .filter(x => x.length),
    end: encodePrononciation(end.join(','))
      .split(',')
      .filter(x => x.length)
  }
}

function generateFindRegex ({
  exclusions = { init: [], median: [], end: [] },
  inclusions = { init: [], median: [], end: [] }
}: { exclusions?: Positions; inclusions?: Positions } = {}) {
  const encodedExclusions = encodePositions(exclusions)
  const encodedInclusions = encodePositions(inclusions)

  const alpha = [...ENCODED_CONSONANTS, ...ENCODED_VOWELS].join('')
  console.log('#########FUCK', { encodedExclusions, encodedInclusions, alpha })

  let initialPart = ''
  let medianPart = `[${alpha}]*`
  if (encodedInclusions.median && encodedInclusions.median.length) {
    medianPart += `[${encodedInclusions.median.join('')}]{1}[${alpha}.]*`
  }
  initialPart =
    '^' +
    createDelimiter(encodedInclusions.init, encodedExclusions.init, alpha) +
    '.*'
  const endPart =
    createDelimiter(encodedInclusions.end, encodedExclusions.end, alpha) + '$'
  return new RegExp(initialPart + medianPart + endPart, 'i')
}

module.exports = {
  index: (req: Request, res: Response, next: NextFunction) => {
    Word.find({ text: /^a/ })
      .then(words => {
        return res.json(words)
      })
      .catch(next)
  },
  find: (req: Request, res: Response, next: NextFunction) => {
    Word.findById(req.query.id)
      .then(async (word: any) => {
        const x = word?._doc.text
        let images: ImageFormattedResponse[] = []
        if (word && word._doc && word._doc.text) {
          images = await getImagesForAWord(word._doc.text)
        }
        return res.json({ word, images })
      })
      .catch(next)
  },
  // updateWords: (req: Request, res: Response, next: NextFunction) => {
  //   Word.find({})
  //     .then(async words => {
  //       const lgt = words.length

  //       for (let i = 0; i < lgt; i++) {
  //         const data = buildWordData(words[i])
  //         const word = words[i]
  //         await Word.findByIdAndUpdate({ _id: word._id }, data)
  //       }
  //       return words
  //     })
  //     .catch(next)
  // },
  search: async (req: Request, res: Response, next: NextFunction) => {
    const {
      pagination: { take = DEFAULT_PAGINATION_TAKE, skip = 0 } = {},
      sort = { freq: -1 },
      filters = {},
      params,
      options: { saveInHistory = false } = {}
    } = req.body
    // Filters
    const filtersTextRegexArray = getFiltersRegexArray(filters?.text || {})
    const filtersRangesArray = getRangeFiltersArray(filters?.ranges || {})

    // Generate regex
    const findRegex = generateFindRegex(params)
    console.log('FINDE REGEX', findRegex)

    // Where ?
    const where = params?.structure ? { structure: params.structure || '' } : {}

    const words = await Word.find({
      $and: [
        ...filtersTextRegexArray,
        ...filtersRangesArray,
        {
          encodedPron: {
            $regex: findRegex
          }
        }
      ]
    })
      .limit(take)
      .skip(skip)
      .where(where as any)
      .sort({
        ...sort
      })

    // Save search in history
    const user: any = req.user
    if (saveInHistory && user) {
      let searchData = {
        user: user.id,
        isSaved: false,
        structure: params.structure,
        inclusions: params.inclusions,
        exclusions: params.exclusions,
        filters: filters,
        nbResults: words?.length || 0,
        hash: ''
      }
      await recordSearchInHistory(
        {
          isSaved: false,
          structure: params.structure,
          inclusions: params.inclusions,
          exclusions: params.exclusions,
          filters: filters
        },
        user,
        words.length
      )
    }

    return res.json({ words })
  },
  generateFindRegex,
  getFiltersRegexArray
}
