import { Request, Response } from 'express'

import mongoose from 'mongoose'

const Search = mongoose.model('Search')

export default {
  getAll: async (req: Request, res: Response) => {
    const user: any = req.user
    const searches = await Search.find({ user: user.id })
    return res.json(searches)
  },
  getLatest: async (req: Request, res: Response) => {
    const user: any = req.user
    const { nb = 10 } = req.params
    const searches = await Search.find({ user: user.id })
      .limit(+nb)
      .sort({ createdAt: 'desc' })
    return res.json(searches)
  },
  deleteHistory: async (req: Request, res: Response) => {
    const user: any = req.user
    const searches = await Search.deleteMany({
      user: user.id,
      isSaved: false
    }).exec()
    return res.json(searches)
  }
}
