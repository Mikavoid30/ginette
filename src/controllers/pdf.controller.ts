import { Request, Response } from 'express'
import PdfService from '../services/pdf'
import mongoose from 'mongoose'
import shortid from 'shortid'
import { ensureDirectoryExistence } from '../helpers/filesystem'
import FileManager from '../services/fileManager'

const Word = mongoose.model('Word')
const pdfService = new PdfService()
const fileManager = new FileManager()

export default {
  createPdfFromList: async (req: Request, res: Response) => {
    const { ids } = req.body
    try {
      const words = await Word.find({
        _id: {
          $in: ids
        }
      })
      const imagesDir = 'data/_TMP_/images'
      await ensureDirectoryExistence(imagesDir)
      const token = shortid()
      //   for (let i = 0; i < words.length; i++) {
      //     const w: any = words[i]
      //     const images = await getImagesForAWord(w._doc.text)

      //     if (images && images.length) {
      //       const x = await fileManager.downloadImageAndAutoRemove(
      //         images[0].previewURL,
      //         `${imagesDir}/img-${w._id}.jpg`
      //       )
      //     }
      //     wordsWithImages.push({ ...w._doc, images })
      //   }

      const fileToken = await pdfService.generatePdf(words, token)
      res.json({ token: fileToken })
    } catch (e) {
      console.log(e)
      res.send(400)
    }
  },

  download: (req: Request, res: Response) => {
    const token = req.params.token || ''
    setTimeout(() => {
      try {
        return res.download(
          'data/_TMP_/' + token + '.pdf',
          `phonemia-export-${new Date().toLocaleDateString()}.pdf`
        )
      } catch (e) {
        return res.status(404).send()
      }
    }, 1000)
  }
}
