// Structures
export const ENCODE_MAPPING = [
  ['ɑ̃', '1'],
  ['ɑ', '0'],
  ['ɔ̃', '3'],
  ['ɔ', '2'],
  ['ɛ̃', '5'],
  ['ε', '4'],
  ['ø', '6'],
  ['œ̃', '8'],
  ['œ', '7'],
  ['ə', '9'],
  ['ʃ', '@'],
  ['ʒ', '&'],
  ['l', '='],
  ['ɲ', '#'],
  ['ɥ', ':']
]

export const VALID_STRUCTURE_REGEX = /^[CV]*(-[CV]*)*[CV]$/
export const CONSONANTS = [
  'b',
  'd',
  'f',
  'ɡ',
  'k',
  'l',
  'm',
  'n',
  'ɲ',
  'p',
  'ʁ',
  's',
  'ʃ',
  't',
  'v',
  'z',
  'ʒ',
  'j',
  'w',
  'ɥ'
]

export const ENCODED_CONSONANTS = CONSONANTS.map(c => {
  const tuple = ENCODE_MAPPING.find(tuple => tuple[0] === c)
  if (tuple) {
    return tuple[1]
  }

  return c
})

export const VOWELS = [
  'a',
  'ɑ',
  'e',
  'ɛ',
  'ɛː',
  'ə',
  'i',
  'œ',
  'ø',
  'o',
  'ɔ',
  'u',
  'y',
  'ɑ̃',
  'ɛ̃',
  'œ̃',
  'ɔ̃'
]

export const ENCODED_VOWELS = VOWELS.map(c => {
  const tuple = ENCODE_MAPPING.find(tuple => tuple[0] === c)
  if (tuple) {
    return tuple[1]
  }

  return c
})

export const BING_API_URL = 'https://bing-image-search1.p.rapidapi.com'
export const BING_SECRET = process.env.BING_SECRET
export const IMAGES_API_URL = 'https://pixabay.com/api/'
export const IMAGES_API_KEY = process.env.IMAGES_API_KEY
export const PEXELS_API_URL = 'https://api.pexels.com/v1'
export const PEXELS_SECRET = process.env.PEXELS_SECRET
export const WIKIPEDIA_URL = 'http://fr.wiktionary.org/w/api.php'
export const WIKI_URL = 'http://fr.{service}.org/w/api.php'
export const WIKI_SERVICES = {
  WIKTIONARY: 'wiktionary',
  WIKIPEDIA: 'wikipedia'
}
export const DEFAULT_PAGINATION_TAKE = 650
export const WIKIPEDIA_THUMB_SIZE = 500

export const JOKES = [
  'sportif',
  'sportive',
  'joggeur',
  'joggeuse',
  'course',
  'sprint',
  'marathon',
  'athlétisme'
]

export enum IMAGES_SERVICES {
  PIXABAY = 'pixabay',
  BING = 'bing',
  PEXELS = 'pexels',
  WIKIPEDIA = 'wiki'
}
