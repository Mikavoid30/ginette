export type ImageFormattedResponse = {
  webformatURL: string
  largeImageURL: string
  previewURL: string
}

export type BingResult = {
  contentUrl: string
  thumbnailUrl: string
}

export type PixabayResult = {
  webformatURL: string
  largeImageURL: string
  previewURL: string
}

export type PexelsResult = {
  src: { large: string; tiny: string }
}
