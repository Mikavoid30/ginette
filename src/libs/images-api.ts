import {
  ImageFormattedResponse,
  BingResult,
  PixabayResult,
  PexelsResult
} from './image-api.types'
import axios from 'axios'
import {
  IMAGES_API_URL,
  IMAGES_API_KEY,
  IMAGES_SERVICES,
  BING_API_URL,
  BING_SECRET,
  PEXELS_API_URL,
  PEXELS_SECRET,
  WIKIPEDIA_THUMB_SIZE,
  WIKIPEDIA_URL,
  WIKI_SERVICES,
  WIKI_URL,
  JOKES
} from '../constants'

const URL = IMAGES_API_URL
const KEY = IMAGES_API_KEY

const USED_API = IMAGES_SERVICES.WIKIPEDIA

async function getWikiImage (
  url = WIKIPEDIA_URL,
  query = ''
): Promise<ImageFormattedResponse[]> {
  try {
    const wikiresult = await axios.get(url, {
      params: {
        action: 'query',
        prop: 'pageimages',
        format: 'json',
        titles: query,
        pithumbsize: WIKIPEDIA_THUMB_SIZE
      }
    })

    const pages = wikiresult.data.query.pages
    if (Object.keys(pages)[0] === '-1') return []

    const firstPage = pages[Object.keys(pages)[0]]
    if (!firstPage.thumbnail) return []
    return [
      {
        webformatURL: firstPage.thumbnail.source,
        largeImageURL: firstPage.thumbnail.source,
        previewURL: firstPage.thumbnail.source
      }
    ]
  } catch (e) {
    console.error(e)
    return []
  }
}

async function getImagesFromService (service: IMAGES_SERVICES, query: string) {
  switch (service) {
    case IMAGES_SERVICES.PIXABAY:
      const results: { data: { hits: PixabayResult[] } } = await axios.get(
        `${URL}`,
        {
          params: {
            key: KEY,
            q: query,
            safesearch: true,
            lang: 'fr',
            image_type: 'photo'
          }
        }
      )
      return results.data.hits.map(x => ({
        webformatURL: x.webformatURL,
        largeImageURL: x.largeImageURL,
        previewURL: x.previewURL
      }))
    case IMAGES_SERVICES.BING:
      const bingResults: { data: { value: BingResult[] } } = await axios.get(
        `${BING_API_URL}/images/search`,
        {
          headers: {
            'x-rapidapi-key': BING_SECRET,
            'x-rapidapi-host': 'bing-image-search1.p.rapidapi.com',
            useQueryString: true
          },
          params: {
            q: query,
            count: 1,
            mkt: 'fr-FR',
            safeSearch: 'Strict'
          }
        }
      )
      return bingResults.data.value.map(x => ({
        webformatURL: x.contentUrl,
        largeImageURL: x.contentUrl,
        previewURL: x.thumbnailUrl
      }))
    case IMAGES_SERVICES.PEXELS:
      const pexelsResults: {
        data: { photos: PexelsResult[] }
      } = await axios.get(`${PEXELS_API_URL}/search`, {
        headers: {
          Authorization: PEXELS_SECRET
        },
        params: {
          query,
          per_page: 1
        }
      })
      return pexelsResults.data.photos.map(x => ({
        webformatURL: x.src.large,
        largeImageURL: x.src.large,
        previewURL: x.src.tiny
      }))
    case IMAGES_SERVICES.WIKIPEDIA:
      let wikiResults = []
      wikiResults = await getWikiImage(
        WIKI_URL.replace('{service}', WIKI_SERVICES.WIKTIONARY),
        query
      )

      if (!wikiResults || !wikiResults.length) {
        wikiResults = await getWikiImage(
          WIKI_URL.replace('{service}', WIKI_SERVICES.WIKIPEDIA),
          query
        )
      }

      return wikiResults
  }
}

export async function getImages ({ q }: { q: string }) {
  try {
    let res = await getImagesFromService(USED_API, q)
    if (JOKES.includes(q.toLowerCase())) {
      res.unshift({
        webformatURL: '/images/sportif.jpg',
        largeImageURL: '/images/sportif.jpg',
        previewURL: '/images/sportif.jpg'
      })
    }
    return res
  } catch (e) {
    return []
  }
}
export async function getImagesForAWord (
  text: string = ''
): Promise<ImageFormattedResponse[]> {
  if (!text) return Promise.resolve([])
  return await getImages({ q: text })
}
