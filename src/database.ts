import mongoose, { Mongoose } from 'mongoose'
require('./models')

export async function connectDB () {
  const dbUser = process.env.MONGODB_USER || ''
  const dbPassword = process.env.MONGODB_PASS || ''
  const dbHost = process.env.MONGODB_HOST || ''
  const dbName = process.env.MONGODB_DATABASE || ''
  let prefix = 'mongodb+srv'
  let suffix = ''

  if (process.env.NODE_ENV !== 'production') {
    prefix = 'mongodb'
    suffix = '?authSource=admin'
    //`mongodb://${dbUser}:${dbPassword}@${dbHost}/${dbName}`,
    console.warn('Connecting to DEV mongodb', dbHost)
  }
  try {
    await mongoose.connect(
      `${prefix}://${dbUser}:${dbPassword}@${dbHost}/${dbName}${suffix}`,
      {
        w: 'majority',
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    )
    return mongoose
  } catch (e) {
    console.error(e)
  }
}
