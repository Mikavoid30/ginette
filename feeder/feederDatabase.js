const mongoose = require('mongoose')

module.exports = async function connectDB () {
  const dbUser = process.env.MONGODB_USER || 'phonemia'
  const dbPassword = process.env.MONGODB_PASS || 'phonemiadev'
  const dbHost = process.env.MONGODB_HOST || 'localhost'
  const dbName = process.env.MONGODB_DATABASE || 'words'
  let prefix = 'mongodb+srv'
  let suffix = ''

  if (process.env.NODE_ENV !== 'production') {
    prefix = 'mongodb'
    suffix = '?authSource=admin'
    console.warn('Connecting to DEV mongodb', dbHost)
  }

  try {
    console.log(`${prefix}://${dbUser}:${dbPassword}@${dbHost}/${dbName}`)
    await mongoose.connect(
      `${prefix}://${dbUser}:${dbPassword}@${dbHost}/${dbName}${suffix}`,
      {
        w: 'majority',
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    )
    return Promise.resolve(mongoose)
  } catch (e) {
    console.error('er', e)
  }
}
