const connectDB = require('./feederDatabase')
const {
  getAllLocalWords,
  buildDico,
  removeComposedWords,
  addPronounctiationToWords,
  addAoA,
  addPron,
  saveWordsToLocalFile
} = require('./scrapper')
const { vs, cs, vscodes, cscodes, encodeMapping } = require('./constants')

function encodePrononciation (decodedPron = '') {
  let encoded = decodedPron

  if (!encoded) return ''

  for (let i = 0; i < encodeMapping.length; i++) {
    encoded = encoded.replace(
      new RegExp(encodeMapping[i][0], 'g'),
      encodeMapping[i][1]
    )
  }

  return encoded
}

function decodePrononciation (encoded = '') {
  let decoded = encoded

  if (!decoded) return ''

  const mapping = encodeMapping
  for (let i = 0; i < mapping.length; i++) {
    decoded = decoded.replace(
      new RegExp(mapping[i][1], 'g'),
      encodeMapping[i][0]
    )
  }

  return decoded
}

// connect to DB
async function main () {
  const off = -22
  let words = await getAllLocalWords()
  // to remove
  // words = words.slice(0, 100)

  words = await removeComposedWords(words)
  words = await addAoA(words)
  words = await addPron(words)

  // remove first line
  words = words.slice(1)

  // saveWordsToLocalFile(words)

  connectDB().then(db => {
    const Word = db.model('Word', {
      text: {
        type: String
      },
      pron: {
        type: String
      },
      encodedPron: {
        type: String
      },
      syllabs: {
        type: Number
      },
      structure: {
        type: String
      },
      freqLem: {
        type: Number
      },
      freq: {
        type: Number
      },
      type: {
        type: String
      },
      aoaMean: {
        type: Number
      },
      aoaSd: {
        type: Number
      },
      freqSubMean: {
        type: Number
      },
      freqSubSd: {
        type: Number
      }
    })
    let count = 0
    let invalids = []
    for (let i = 0, lgt = words.length; i < lgt; i++) {
      const pron = words[i].pron
      const encoded = encodePrononciation(pron)
      const decoded = decodePrononciation(encoded)
      pron !== decoded && invalids.push({ encoded, decoded, pron })
      const newWord = new Word(addStructure(words[i]))
      newWord
        .save()
        .then(() => count++)
        .catch(e => console.log(e))
      console.clear()
      console.log('SAVED ' + i)
    }
    console.log('INVALIDS ENCODED ', invalids)
  })
}

function addStructure (word) {
  const { pron, text } = word
  if (!pron || !text) return

  const syllabs = pron.split('.')
  const nbSyllabls = syllabs.length

  const struct = syllabs
    .map(w => {
      return [...w]
        .map(char =>
          vscodes.includes(char.charCodeAt(0))
            ? 'V'
            : cscodes.includes(char.charCodeAt(0))
            ? 'C'
            : char.charCodeAt(0) === 771
            ? ''
            : 'X'
        )
        .join('')
    })
    .join('-')

  return {
    ...word,
    syllabs: nbSyllabls,
    structure: struct,
    encodedPron: encodePrononciation(pron)
  }
}

main()
