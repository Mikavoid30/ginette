const encodeMapping = [
  ['ɑ̃', '1'],
  ['ɑ', '0'],
  ['ɔ̃', '3'],
  ['ɔ', '2'],
  ['ɛ̃', '5'],
  ['ε', '4'],
  ['ø', '6'],
  ['œ̃', '8'],
  ['œ', '7'],
  ['ə', '9'],
  ['ʃ', '@'],
  ['ʒ', '&'],
  ['l', '='],
  ['ɲ', '#'],
  ['ɥ', ':']
]

const vs = [
  'a',
  'e',
  'i',
  'o',
  'u',
  'a',
  'ɛ',
  'ɔ',
  'y',
  'ə',
  'œ',
  'ø',
  'ɑ̃',
  'ɔ̃',
  'ɛ̃',
  'œ̃'
]
const cs = [
  'b',
  'd',
  'ɡ',
  'ɡ',
  'v',
  'z',
  'ʒ',
  'p',
  't',
  'k',
  'f',
  's',
  'ʃ',
  'm',
  'n',
  'ɲ',
  'ŋ',
  'ʁ',
  'l',
  'j',
  'ɥ',
  'w'
]

module.exports.consonants = [
  'b',
  'd',
  'f',
  'ɡ',
  'k',
  'l',
  'm',
  'n',
  'ɲ',
  '',
  'ʁ',
  's',
  'ʃ',
  't',
  'v',
  'z',
  'ʒ',
  'j',
  'w',
  'ɥ'
]
module.exports.vowels = [
  'a',
  'ɑ',
  'e',
  'ɛ',
  'ɛː',
  'ə',
  'i',
  'œ',
  'ø',
  'o',
  'ɔ',
  'u',
  'y',
  'ɑ̃',
  'ɛ̃',
  'œ̃',
  'ɔ̃'
]

const vscodes = vs.map((x, i) => x.charCodeAt(0))
const cscodes = cs.map((x, i) => x.charCodeAt(0))

module.exports.vs = vs
module.exports.vscodes = vscodes
module.exports.cs = cs
module.exports.cscodes = cscodes
module.exports.encodeMapping = encodeMapping
