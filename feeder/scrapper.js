const axios = require('axios')
const cheerio = require('cheerio')
const fakeUa = require('fake-useragent')
const xlsx = require('node-xlsx')
const fs = require('fs')
const { randomDelay, removeSlashes, delay, randomNumber } = require('./helpers')

function isComposed (w = '') {
  if (w.trim().includes(' ')) return true
  if (w.trim().includes('-')) return true
  if (w.trim().includes('’')) return true
  if (w.trim().includes("'")) return true

  return false
}

function getAllLocalWords () {
  const path = __dirname + '/data/lexique.xlsx'

  const obj = xlsx.parse(path)
  const initialWords = obj[0].data.map(x => ({
    text: x[0],
    freqLem: x[4],
    freq: x[5],
    type: x[3]
  }))
  return initialWords
}

function addAoA (words) {
  const path = __dirname + '/data/lexique-aoa.xlsx'
  const obj = xlsx.parse(path)
  return words.map(x => {
    const w = obj[0].data.find(
      word => word[0].toLowerCase() === x.text.toLowerCase()
    )
    if (w) {
      // console.log('found aoa', x, w)
      return {
        ...x,
        aoaMean: w[1],
        aoaSd: w[2],
        freqSubMean: w[3],
        freqSubSd: w[4]
      }
    }
    return {
      ...x,
      aoaMean: null,
      aoaSd: null,
      freqSubMean: null,
      freqSubSd: null
    }
  })
}

function addPron (words) {
  const path = __dirname + '/data/prons.xlsx'
  const obj = xlsx.parse(path)
  return words.map((x, i) => {
    console.clear()
    console.log('pron ' + i + '/' + words.length)
    const w = obj[0].data.find(
      word => word[0].toLowerCase() === x.text.toLowerCase()
    )
    if (w) {
      // console.log('found pron', x, w)
      return {
        ...x,
        pron: w[4]
      }
    }
    return {
      ...x,
      pron: null
    }
  })
}

function getWordRow (word) {
  return [
    word.text,
    word.freqLem,
    word.freq,
    word.type,
    word.pron,
    word.aoaMean,
    word.aoaSd,
    word.freqSubMean,
    word.freqSubSd
  ]
}

function removeComposedWords (words) {
  const initalCount = words.length
  const excluded = []
  const filtered = words.filter(x => {
    if (isComposed(x.text)) {
      excluded.push(getWordRow(x))
      return false
    }
    return true
  })
  const finalCount = filtered.length
  console.log(initalCount - finalCount + ' words has been filtered')
  saveWordsToLocalFile(excluded, 'excluded')
  return filtered
}

async function addPronounctiationToWords (words) {
  const dico = await buildDico(words)
  return dico.map(x => {
    x = { ...x, pron: x.pron }
    return getWordRow(x)
  })
}

function saveWordsToLocalFile (words, title = 'save') {
  const buffer = xlsx.build([{ name: title, data: words }], {})

  fs.open(`${__dirname}/data/generated/${title}.xlsx`, 'w', function (err, fd) {
    if (err) {
      throw 'could not open file: ' + err
    }

    // write the contents of the buffer, from position 0 to the end, to the file descriptor returned in opening our file
    fs.write(fd, buffer, 0, buffer.length, null, function (err) {
      if (err) throw 'error writing file: ' + err
      fs.close(fd, function () {
        console.log('wrote the file successfully')
      })
    })
  })
}

// async function getAllWords () {
//   return [
//     'de',
//     'la',
//     'le'
//     // 'et',
//     // 'les',
//     // 'des',
//     // 'en',
//     // 'un',
//     // 'du',
//     // 'une',
//     // 'que',
//     // 'est',
//     // 'pour',
//     // 'qui',
//     // 'dans',
//     // 'par',
//     // 'plus',
//     // 'pas',
//     // 'au',
//     // 'sur',
//     // 'ne',
//     // 'se',
//     // 'Le',
//     // 'ce',
//     // 'il',
//     // 'sont',
//     // 'La',
//     // 'Les',
//     // 'ou',
//     // 'avec'
//   ]
// }

async function buildDico (words = []) {
  const dico = []
  const errors = []
  // words = words.slice(0, 20)
  let bigPauseIndex = randomNumber(5, 50)
  for (let i = 0, lgt = words.length; i < lgt; i++) {
    await randomDelay()
    if (i % bigPauseIndex === 0) {
      console.log(bigPauseIndex + 'has passed, big pause')
      bigPauseIndex = randomNumber(5, 50)
      await randomDelay(20000, 40000)
    }
    const currentWord = words[i]
    console.log(
      'Getting prononciation for ',
      currentWord.text,
      `${i + 1} / ${lgt}`
    )
    const url = `https://fr.wiktionary.org/wiki/${encodeURI(
      currentWord.text.trim().toLowerCase()
    )}`
    try {
      const response = await axios.get(url, {
        headers: { 'User-Agent': fakeUa() }
      })
      const $ = cheerio.load(response.data)

      const prononciation = $('p span.API')
        .first()
        .text()
      console.log('>>>', prononciation)

      if (prononciation && prononciation.length)
        dico.push({
          text: currentWord.text.trim(),
          pron: removeSlashes(prononciation)
        })
    } catch (e) {
      console.log('error with', currentWord.text)
      errors.push([currentWord.text])
      await delay(1)
      continue
    }
  }
  saveWordsToLocalFile(errors, 'nopronfound')
  return dico
}
async function getAllWords () {
  const cheerio = require('cheerio')
  const axios = require('axios')

  const pages = [
    'https://fr.wiktionary.org/wiki/Wiktionnaire:Listes_de_fr%C3%A9quence/wortschatz-fr-1-2000',
    'https://fr.wiktionary.org/wiki/Wiktionnaire:Listes_de_fr%C3%A9quence/wortschatz-fr-2001-4000',
    'https://fr.wiktionary.org/wiki/Wiktionnaire:Listes_de_fr%C3%A9quence/wortschatz-fr-4001-6000',
    'https://fr.wiktionary.org/wiki/Wiktionnaire:Listes_de_fr%C3%A9quence/wortschatz-fr-6001-8000',
    'https://fr.wiktionary.org/wiki/Wiktionnaire:Listes_de_fr%C3%A9quence/wortschatz-fr-8001-10000'
  ]

  const list = []
  for (let p = 0; p < pages.length; p++) {
    const response = await axios.get(pages[p])
    const $ = cheerio.load(response.data)

    const items = $('.mw-parser-output li')

    // We now loop through all the elements found

    for (let i = 0; i < items.length; i++) {
      const w = $(items[i])
        .find('a')
        .text()
        .trim()
      if (isComposed(w)) continue
      if (w.length >= 2) list.push(w)
    }
  }

  return list
}

module.exports.getAllWords = getAllWords
module.exports.getAllLocalWords = getAllLocalWords
module.exports.removeComposedWords = removeComposedWords
module.exports.addPronounctiationToWords = addPronounctiationToWords
module.exports.saveWordsToLocalFile = saveWordsToLocalFile
module.exports.buildDico = buildDico
module.exports.addAoA = addAoA
module.exports.addPron = addPron
