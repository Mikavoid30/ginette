const { vs, cs, vscodes, cscodes } = require('../constants')

module.exports.randomNumber = function randomNumber (min, max) {
  return Math.random() * (max - min) + min
}
module.exports.randomDelay = function randomDelay (min = 200, max = 500) {
  const random = Math.random() * (max - min) + min
  console.log('# DELAY ' + random)
  return new Promise(resolve => {
    setTimeout(resolve, random)
  })
}

module.exports.delay = function delay (s) {
  const random = s * 1000
  return new Promise(resolve => {
    setTimeout(resolve, random)
  })
}

module.exports.removeSlashes = function removeSlashes (str = '') {
  return str.split('\\').join('')
}

module.exports.buildWordData = function buildWordData (word) {
  const accents = [771]
  const pron = word.pron
  const syllabs = pron.split('.')
  const nbSyllabs = syllabs.length

  const structure = syllabs
    .map(w => {
      return [...w]
        .map(char =>
          vscodes.includes(char.charCodeAt(0))
            ? 'V'
            : cscodes.includes(char.charCodeAt(0))
            ? 'C'
            : accents.includes(char.charCodeAt(0))
            ? ''
            : 'X'
        )
        .join('')
    })
    .join('-')

  return { structure, syllabs: nbSyllabs, text: word }
}

// NOTES

/**
 *
 * ^[bdfɡklmnɲpʁsʃtvzʒjwɥ][aɑeɛɛːəiœøoɔuyɑ̃ɛ̃œ̃ɔ̃][bdfɡklmnɲpʁsʃtvzʒjwɥ]\.[bdfɡklmnɲpʁsʃtvzʒjwɥ][aɑeɛɛːəiœøoɔuyɑ̃ɛ̃œ̃ɔ̃][bdfɡklmnɲpʁsʃtvzʒjwɥ]$
 * https://fr.wikipedia.org/wiki/Aide:Alphabet_phon%C3%A9tique_fran%C3%A7ais
 */
